import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import HideableText from './HideableText';
import AutoCompleteText from './AutoCompleteTextRemonteState';
import categorie from './Categorie'
import aliments from './Aliments.js';
import logo from './logo_final.png';
//import AlimentsKcal from './AlimentsKcal'


class App extends React.Component {
  constructor(props) {

    super(props);
    this.state = {
      AutoCompleteElement: React.createRef(),
      quantity: 1,
      list: [],
      text: '',
      todisplay:'',
      /*listVitale:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      RecommendationsHomme:[0,3000,0,0,0,'-','-',30,'-','-','-','-','8-8.3','1.3-1.6','-','-',1000,2300,"1-1.5",10,"150-200",350,"2-5",700,4000,70,1500,"11-16",6,"2-4",14,70,110,1.2,1.4,15,6,1.6,300,4],
      RecommendationsFemme:[0,3000,0,0,0,'-','-',30,'-','-','-','-','5-6.6','1-1.3','-','-',1000,2300,"1-1.5",15,"150-200",300,"2-5",700,4000,60,1500,"7-10",4.8,"2-4",12,60,95,1,1.1,12,6,1.4,300,4],
      Recommendations:[0,3000,0,0,0,'-','-',30,'-','-','-','-','5-6.6','1-1.3','-','-',1000,2300,"1-1.5",15,"150-200",300,"2-5",700,4000,60,1500,"7-10",4.8,"2-4",12,60,95,1,1.1,12,6,1.4,300,4],
      Pourcentages:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],*/
      
      listVitale:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      RecommendationsHomme:[0,3000,0,0,0,30,8.15,1.45,1000,2300,1.25,10,175 ,350,3.5,700,4000,70,1500,13.5,6,3,14,70,110,1.2,1.4,15,6,1.6,300,4],
      RecommendationsFemme:[0,3000,0,0,0,30,5.8,1.15,1000,2300,1.25,15,175,300,3.5,700,4000,60,1500,8.5,4.8,3,12,60,95,1,1.1,12,6,1.4,300,4],
      Recommendations:[0,3000,0,0,0,30,5.8,1.15,1000,2300,1.25,15,175,300,3.5,700,4000,60,1500,8.5,4.8,3,12,60,95,1,1.1,12,6,1.4,300,4],
      Pourcentages:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      

      checked:"femme",
      high:1.8,
      weight:80,
      years:30,
      sedentar:1.37,
  
    };
    this.onRadioChange = this.onRadioChange.bind(this)
 
  }

  updateHigh(value){
    this.setState({
      high:(value/100)
    })
    //this.updateRecommendation()
  }

  updateWeight(value){
    this.setState({
      weight:value
    })
    //this.updateRecommendation()
  }

  updateYears(value){
    this.setState({
      years:value
    })
    //this.updateRecommendation()
  }


  updateRecommendation() {
    var weight=this.state.weight;
    var high=this.state.high;
    var years=this.state.years;
    var genre = this.state.checked;
    var actif = this.state.sedentar;
    if (genre==="femme"){
      var mb = Math.round(((9.5634*weight)+(184.96*high) - (4.6756*years) + 655.0955)*actif)
      var recommendationValue = this.state.RecommendationsFemme
      }  
    else{    
    var mb = Math.round(((13.7516*weight)+(500.33*high) - (6.7550*years) + 66.473)*actif)
    var recommendationValue = this.state.RecommendationsHomme
      
    }
    recommendationValue[0]=mb
    recommendationValue[2]=Math.round((mb*0.20)/4)
    recommendationValue[3]=Math.round((mb*0.50)/4)
    recommendationValue[4]=Math.round((mb*0.3)/9)

    this.setState({
      Recommendations: recommendationValue})
  }

    onRadioChange(e) {
    //console.log(e.target.value)
    
    this.setState({
      checked:e.target.value
      //[e.target.name]: e.target.value
    })
    //this.updateRecommendation(e.target.value) 

  }

  UpdateText=(value)=>{
  	
  	this.setState(() => ({text: value}));
 
  }

  updateQuantity(value) {
    // update react state
    //const valuepourcentage=(this.value)/100
    if (isNaN(value*1)){value=0}
    this.setState({ quantity: value/100 });
  }


  UpdateVitality(name,quantity,bool) {
   
    // Take back the list of quantity
    const newVitale = this.state.listVitale;

    // Find nutrient information of the aliment in the database
    const listName= aliments.map(function(value,index) { return value[0]; });
    const id = listName.findIndex(obj => obj === name);
    const nutrilist=aliments[id];

    // Remove string and name of the nutriment 
    var NaNnumberlist=nutrilist.map(function(number) { return number * 1; });
    var numberlist = NaNnumberlist.map(i =>{ return isNaN(i) ? 0 : i});
    //NaNnumberlist.forEach(function(item, i) { if (isNaN(item)){ return 100;} return item});// NaNnumberlist[i] = 0; })
    //NaNnumberlist.shift();
    numberlist.shift();

    var allCorrectNumberList=numberlist.map(function(number) { return number * quantity; });

    /*var correctNumberList=allCorrectNumberList[1,2,3,4,5,8,13,14,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40]
    */
    var correctNumberList_first=allCorrectNumberList.slice(0,6);
    correctNumberList_first = correctNumberList_first.concat(allCorrectNumberList.slice(8,9));
    correctNumberList_first = correctNumberList_first.concat(allCorrectNumberList.slice(13,15));
    correctNumberList_first = correctNumberList_first.concat(allCorrectNumberList.slice(17,41));
  
    var correctNumberList_pre = correctNumberList_first.map((a, i) => correctNumberList_first[i] * 1);
    var correctNumberList = correctNumberList_pre.map(i =>{ return isNaN(i) ? 0 : i});


    if (bool===1){
     
      var totVitale = newVitale.map((a, i) => Math.round((a + (Math.round(correctNumberList[i] * 100) / 100))* 100) / 100);
    }
      else{
      
        var totVitale = newVitale.map((a, i) => Math.round((a - (Math.round(correctNumberList[i] * 100) / 100))* 100) / 100);
      }


    const recommendations=this.state.Recommendations;
    var porcent= totVitale.map((a, i) => Math.round((a/recommendations[i]) * 100));
    var numberporcent = porcent.map(i =>{ return isNaN(i) ? '-' : i});
    var numberporcent_final = numberporcent.map(i =>{ return i=='Infinity' ? '-' : i});



    this.setState({
      listVitale: totVitale,
      Pourcentages: numberporcent_final
    })  ;


  }
  addItem() {
  	const {text}=this.state;
    this.UpdateVitality(text,this.state.quantity,1);



    // Add name in list
    const newItem = {
      id: 1 + Math.random(),
      aliments: this.state.text, 
      quantity: this.state.quantity
 		
    };

    // copy current list of items
    const list = [...this.state.list];

    // add the new item to the list
    list.push(newItem);

   

    // update state with new list, reset the new item input
    this.setState({
      list,
      text:''  
    });
    
  }
  
  /*updateAutoComplete(text) {
        updateState(text)
    }*/

  deleteItem(element,id_to_delete) {
    // copy current list of items
    const list = [...this.state.list];
    const alimentToDelete = element.aliments;
    this.UpdateVitality(element.aliments,element.quantity,0);
    /*const newVitale = this.state.listVitale;

    //recover information of the aliment to delete in the database
    const listName= aliments.map(function(value,index) { return value[0]; });
    const id_database = listName.findIndex(obj => obj === alimentToDelete.aliments);
    const numberlist = aliments[id_database]

    // Substract nutriment of the aliment to the displayed list
    var correctNumberList=numberlist.map(function(number) { return number * alimentToDelete.quantity; });
    var totVitale = newVitale.map((a, i) => a - correctNumberList[i]);*/

    // filter out the item being deleted
    const updatedList = list.filter(item => item.id !== id_to_delete);

    this.setState({ list: updatedList});
  }



  render() {
    return (
    	<div className="App">
        <div className="entete">
          <a href="https://www.joiedevivres.ch"><img
            className="logo"
            alt=""
            src={logo} 
            height="70px" 
            width="auto" 
              /></a>
          <h1 className="app-title">Equilibre Alimentaire</h1>
        </div>
        <br/><br/>
          <div className="Add-Global">
            <div className="infoPerso">
              <br/>
                <div className="CheckBox">
                  <label className="radiobutton">
                    <input
                      type="radio"
                      name="checked"
                      value="femme"
                      checked={this.state.checked === "femme"}
                      onChange={e => this.onRadioChange(e)}
                    />Femme
                    <span className="genre-text"> </span> <br/>
                   </label>
                   <label className="radiobutton">
                   <input
                      type="radio"
                      name="checked"
                      value="homme"
                      checked={this.state.checked === "homme"}
                      onChange={e => this.onRadioChange(e)}
                    />Homme
                     <span className="genre-text"></span>
                  </label>
                </div>
                <div className="taille">
                  <h2 className="app-text">Taille (cm)</h2>
                  <input 
                    className="tailleItem"
                    placeholder=" 180" 
                    type="int"
                    value={this.high}
                    onChange={e => this.updateHigh(e.target.value)}
                  />
                </div>
                <div className="poids">
                  <h2 className="app-text">Poids (kg)</h2>
                  <input 
                    className="poidsItem"
                    placeholder=" 80" 
                    type="int"
                    value={this.weight}
                    onChange={e => this.updateWeight(e.target.value)}
                  />
                </div>

                <div className="age">
                  <h2 className="app-text">Age</h2>
                  <input 
                    className="ageItem"
                    placeholder=" 30" 
                    type="int"
                    value={this.years}
                    onChange={e => this.updateYears(e.target.value)}
                  />
                </div>
                <button
                  className="add-mb"
                  onClick={() => this.updateRecommendation()}>
                  <i class="material-icons"> calculer </i>
                </button>
            </div>
            <br/> 
            <div className="infoList">
               
            	<div className="App-Component">

            		<AutoCompleteText
                  ref ={this.AutoCompleteElement}
            			items = {aliments.map(function(value,index) { return value[0]; })}
            			onInputChanged={this.UpdateText}/>

               
            	</div>
              <br />
              <div className="Add-Quantity">
                <h2 className="app-text">Quantité (g)</h2>

                  <input 
                          className="quantity"
                          placeholder=" 100" 
                          type="int"
                          value={this.newQuantity}
                          onChange={e => this.updateQuantity(e.target.value)}
                        />
                       <button
                          className="add-btn"
                          onClick={() => this.addItem()}
                          handleClick={()=>this.updateAutoComplete('')}
                          disabled={!this.state.text.length}>
                            <i class="material-icons"> ajouter </i>
                      </button>
                </div>
                 <br/> 
              </div>
             

            <div className="ListAliments">
  	          <ul>
                  {this.state.todisplay}
    	            {this.state.list.map(item => {
    	              return (
    	                <li 
                        className="list"
                        key={item.id}>
    	                  {item.aliments}<br/>              {item.quantity*100}g
    	                  <button  
                          className="delete-btn" 
                          onClick={() => this.deleteItem(item,item.id)}>
    	                    <i class="material-icons"> supprimer </i>
    	                  </button>
                        
    	                </li>
    	              );
    	            })}           
                    
  		          </ul>
  		      </div>
          </div>
          
          <div className="ListResultat">
            <ul>
              <div className="ListNutriments">
                <br />
                <h2 className="list-text">Nutriments</h2>
                        {categorie.map(item=>{return (<li className="list">
                            {item}
                                    </li>);})}
                  <br />
               </div>
               <div className="ListApport">
                  <br />
                   <h2 className="list-text">Mes apports</h2>
                        {this.state.listVitale.map(item =>{
                          return (<li className="list">
                            {item}
                                    </li>);})}
                  <br />
            </div>
             <div className="ListRecommendations">
                  <br />
                   <h2 className="list-text">Mon idéal</h2>
                        {this.state.Recommendations.map(item =>{
                          return (<li className="list">
                            {item}
                                    </li>);})}
                  <br />
            </div>
            <div className="ListPourcentage">
                  <br />
                   <h2 className="list-text">Pourcentages</h2>
                        {this.state.Pourcentages.map(item =>{
                          return (<li className="list">
                            {item}%
                                    </li>);})}
                  <br />
            </div>

            </ul>
          </div>

        </div>
   
    );
  }
}


export default App;
