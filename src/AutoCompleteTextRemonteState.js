import React from 'react';
import './AutoCompleteText.css';

export default class AutoCompleteText extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      suggestions: [],
      text: '',
      quantity:100,
    };
  }

  onTextChanged = (e) => {
    const { items } = this.props
    const value = e.target.value;
    let suggestions = [];
    if (value.length >0){
      const regex = new RegExp(`^${value}`,'i');
      suggestions = items.sort().filter(v => regex.test(v));


    }
    //this.props.onInputChanged(() => ({text: value}));
    this.setState(() => ({suggestions, text: value}));

  }

suggestionSelected (value) {
  this.props.onInputChanged(value);
  this.setState(() => ({suggestions: [], text: value}));


}

removeText = (e) => {
  this.setState(() => ({suggestions: [], text: []}));
}


renderSuggestions () {
  const { suggestions } = this.state;
  if(suggestions.length === 0){
    return null;
  }
  return (
      <ul>
        {suggestions.map((item) => <li onClick={() => this.suggestionSelected(item)}>{item}</li>)}  
      </ul>

    )

  }

  render () {
    const {text} = this.state;
    return (
        <div>
        <br />
          <h2 className="app-text">Choisis un aliment</h2>
          <div className='AutoCompleteText'>
            <input placeholder=" Chercher un aliment" 
                  onClick={this.removeText}
                  value = {text} onChange={this.onTextChanged} type = "text" />
            {this.renderSuggestions()}
          </div>
      </div>
     
      
    )
  }
}
/*<div>
        Choisi un aliment
        <div className='AutoCompleteText'>

          <input placeholder="Chercher un aliment" 
                value = {text} onChange={this.onTextChanged} type = "text" />
          {this.renderSuggestions()}
        </div>
      </div>
*/