import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
//import HideableText from './HideableText';
import AutoCompleteText from './AutoCompleteText';
import AlimentsName from './AlimentsName'
import './AutoCompleteText.css';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newItem: "",
      list: [],
      suggestions: [],
      text: '',
    };
  }

  //incorporating local storage 
  componentDidMount() {
    this.hydrateStateWithLocalStorage();

    // add event listener to save state to localStorage
    // when user leaves/refreshes the page
    window.addEventListener(
      "beforeunload",
      this.saveStateToLocalStorage.bind(this)
    );
  }

  componentWillUnmount() {
    window.removeEventListener(
      "beforeunload",
      this.saveStateToLocalStorage.bind(this)
    );

    // saves if component has a chance to unmount
    this.saveStateToLocalStorage();
  }

  hydrateStateWithLocalStorage() {
    // for all items in state
    for (let key in this.state) {
      // if the key exists in localStorage
      if (localStorage.hasOwnProperty(key)) {
        // get the key's value from localStorage
        let value = localStorage.getItem(key);

        // parse the localStorage string and setState
        try {
          value = JSON.parse(value);
          this.setState({ [key]: value });
        } catch (e) {
          // handle empty string
          this.setState({ [key]: value });
        }
      }
    }
  }

  saveStateToLocalStorage() {
    // for every item in React state
    for (let key in this.state) {
      // save to localStorage
      localStorage.setItem(key, JSON.stringify(this.state[key]));
    }
  }

  updateInput(key, value) {
    // update react state
    this.setState({ [key]: value });
  }

  addItem() {
    // create a new item with unique id
    const newItem = {
      id: 1 + Math.random(),
      value: this.state.text.slice()
 
    };

    // copy current list of items
    const list = [...this.state.list];

    // add the new item to the list
    list.push(newItem);

    // update state with new list, reset the new item input
    this.setState({
      list,
      newItem: ""
    });
  }

  deleteItem(id) {
    // copy current list of items
    const list = [...this.state.list];
    // filter out the item being deleted
    const updatedList = list.filter(item => item.id !== id);

    this.setState({ list: updatedList });
  }

  onTextChanged = (e) => {
    const {names}  = {AlimentsName}
    const value = e.target.value;
    let suggestions = [];
    if (value.length >0){
      const regex = new RegExp(`^${value}`,'i');
      suggestions = names.sort().filter(v => regex.test(v));


    }
    this.setState(() => ({suggestions, text: value}));
  }

suggestionSelected (value) {
  this.setState(() => ({
      text: value,
      suggestions: [],
  }))

}

  renderSuggestions () {
    const { suggestions } = this.state;
    if(suggestions.length === 0){
      return null;
    }
    return (
        <ul>
          {suggestions.map((name) => <li onClick={() => this.suggestionSelected(name)}>{name}</li>)}  
        </ul>

      )

  }
  render() {
    const {text} = this.state;

    return (
      <div>

      <h1 className="app-title">Ma liste d aliments</h1>
        
        <div className="container">
        <div
          style={{
            padding: 30,
            textAlign: "left",
            maxWidth: 500,
            margin: "auto"
          }}
        >
          Choisi un aliment
          <br />

          <div className="App">
            <div className="App-Component">
              <div className='AutoCompleteText'>
                <input value = {text} onChange={this.onTextChanged} type = "text" />
                {this.renderSuggestions()}
              </div>            
            </div>
          </div>

          <input
            type="text"
            placeholder="Chercher un aliment"
            value={this.state.newItem}
            onChange={e => this.updateInput("newItem", e.target.value)}
          />

          <button
            className="add-btn btn-floating"
            onClick={() => this.addItem()}
            disabled={!this.state.newItem.length}
          >
            <i class="material-icons"> ajouter </i>
          </button>
          <br /> <br />
          <ul>
            {this.state.list.map(item => {
              return (
                <li key={item.id}>
                  {item.value}
                  <button className="delete-btn btn-floating" onClick={() => this.deleteItem(item.id)}>
                    <i class="material-icons">supprimer</i>
                  </button>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
      </div>
    );
  }
}


export default App;
     