export default `Céleri rémoulade, préemballé
Salade de thon et légumes, appertisée, égouttée
Salade composée avec viande ou poisson, appertisée, égouttée
Champignon à la grecque
Salade de pommes de terre maison
Taboulé ou Salade de couscous, préemballé
Salade de pomme de terre à la piémontaise, préemballée
Salade de riz
Salade de pâtes, végétarienne
Crudité, sans assaisonnement (aliment moyen)
Salade de pâtes aux légumes, avec poisson ou viande
Salade César au poulet (salade verte, fromage, croûtos, sauce)
Salade de chou ou Coleslaw, avec sauce, préemballée
Taboulé ou Salade de couscous au poulet, préemballé
Soupe aux lentilles, préemballée à réchauffer
Soupe à la volaille et aux légumes, préemballée à réchauffer
Soupe aux légumes variés, préemballée à réchauffer
Soupe de poissons et / ou crustacés, préemballée à réchauffer
Soupe aux légumes variés, déshydratée reconstituée
Soupe aux poireaux et pommes de terre, préemballée à réchauffer
Soupe à la volaille et aux vermicelles, préemballée à réchauffer
Bouillon de viande et légumes type pot-au-feu, prêt à consommer
Soupe à l oignon, préemballée à réchauffer
Soupe aux champignons, préemballée à réchauffer
Soupe à la carotte, préemballée à réchauffer
Soupe à la tomate, préemballée à réchauffer
Soupe chorba frik, à base de viande et de frik
Soupe minestrone, préemballée à réchauffer
Soupe au pistou, déshydratée reconstituée
Soupe de poissons et / ou crustacés, déshydratée reconstituée
Soupe asiatique, avec pâtes, déshydratée reconstituée
Soupe marocaine, déshydratée reconstituée
Soupe aux poireaux et pommes de terre, déshydratée reconstituée
Soupe à la volaille et aux légumes, déshydratée reconstituée
Bouillon de boeuf, déshydraté reconstitué
Soupe aux asperges, déshydratée reconstituée
Soupe à la tomate et aux vermicelles, préemballée à réchauffer
Soupe aux céréales et aux légumes, déshydratée reconstituée
Soupe à la tomate, déshydratée reconstituée
Soupe aux champignons, déshydratée reconstituée
Soupe à l oignon, déshydratée reconstituée
Soupe au potiron, préemballée à réchauffer
Bouillon de volaille, déshydraté reconstitué
Bouillon de légumes, déshydraté reconstitué
Soupe à la tomate et aux vermicelles, déshydratée reconstituée
Soupe à la volaille et aux vermicelles, déshydratée reconstituée
Soupe au pistou, préemballée à réchauffer
Soupe au potiron, déshydratée reconstituée
Soupe asiatique, avec pâtes, préemballée à réchauffer
Soupe minestrone, déshydratée reconstituée
Soupe au cresson, déshydratée reconstituée
Soupe au cresson, préemballée à réchauffer
Soupe aux légumes avec fromage, préemballée à réchauffer
Soupe aux légumes verts, préemballée à réchauffer
Soupe aux légumes verts, déshydratée reconstituée
Soupe aux pois cassés, préemballée à réchauffer
Soupe froide type Gaspacho ou Gazpacho
Soupe aux asperges, préemballée à réchauffer
Soupe (aliment moyen)
Soupe miso
Tripes à la mode de Caen
Tripes à la mode de Caen, préemballées
Tripes à la tomate ou à la provençale
Blanquette de veau
Boeuf bourguignon
Canard en sauce (poivre vert, chasseur, etc.)
Lapin à la moutarde
Coq au vin
Paupiette de veau
Paupiette de volaille
Viande en sauce (aliment moyen)
Poulet au curry et au lait de coco
Meloukhia, plat à base de boeuf et corete
Palette à la diable
Langue de bœuf sauce madère
Porc au caramel
Boulettes au bœuf, à la sauce tomate
Paupiette de veau, cuite au four
Carpaccio de bœuf, avec marinade
Yakitori (brochettes japonaises grillées en sauce)
Hachis Parmentier à la viande
Couscous au mouton
Couscous à la viande ou au poulet, allégé
Poêlée de pommes de terre préfrites, lardons ou poulet, et autres, sans légumes verts
Couscous royal (avec plusieurs viandes)
Couscous au poulet
Couscous à la viande
Parmentier de canard
Parmentier de canard, cuit
Cassoulet, appertisé
Choucroute garnie
Petit salé ou saucisse aux lentilles
Pot-au-feu
Boeuf aux carottes
Potée auvergnate (chou et porc)
Cassoulet au porc, appertisé
Cassoulet au canard ou oie, appertisé
Tomate farcie
Chou farci
Chili con carne
Moussaka
Navarin d agneau aux légumes
Tajine de mouton
Osso buco
Poulet basquaise
Tajine de poulet
Chop suey (porc ou poulet)
Légumes farcis (sauf tomate)
Moules marinières (oignons et vin blanc)
Moules farcies (matière grasse, persillade…), crues
Gratin ou cassolette de poisson et / ou fruits de mer, à cuire
Gratin ou cassolette de poisson et / ou fruits de mer, cuit
Saumon à l oseille
Poisson blanc à la bordelaise
Poisson blanc à la provençale ou niçoise (sauce tomate)
Poisson blanc à la florentine (sauce aux épinards)
Poisson blanc à la marinière (sauce aux oignons, vin blanc, moules)
Poisson blanc à la sauce moutarde
Poisson blanc à la parisienne (sauce aux champignons)
Poisson blanc à l estragon
Poisson blanc sauce oseille
Saumon farci
Brochette de poisson
Brochette de crevettes
Poisson en sauce, surgelé
Poisson cuit (aliment moyen)
Poisson blanc, cuit (aliment moyen)
Poisson blanc, de mer, cuit (aliment moyen)
Paëlla
Couscous au poisson
Gratin de poisson et purée ou brandade aux pommes de terre ou parmentier de poisson
Sushi ou Maki aux produits de la mer
Terrine ou mousse de légumes
Flan de légumes
Haricots blancs à la sauce tomate, appertisés
Piperade basquaise
Poêlée de légumes assaisonnés sans champignon, surgelée, crue
Poêlée de légumes assaisonnés à l asiatiques ou wok de légumes, surgelée, crue
Poêlée de légumes assaisonnés grillée, méridionale ou méditerranéenne, surgelée, crue
Poêlée de légumes assaisonnés aux champignons ("champêtre"), surgelée
Tofu, nature
Escalope végétale ou steak à base de soja
Choucroute, sans garniture, égouttée
Ratatouille cuisinée
Épinards à la crème
Gratin d aubergine
Gratin dauphinois
Endive au jambon
Gratin de chou-fleur
Gratin de légumes
Riste d aubergines (aubergines, tomates, oignons)
Palet ou galette de légumes, préfrit, surgelé
Palet ou galette de légumes, préfrit, surgelé, cuit
Choucroute, sans garniture, égouttée, cuite
Tomate à la provençale
Beignet de légumes
Falafel ou Boulette de pois-chiche et/ou fève, frite
Gratin de légumes en sauce blanche type béchamel, cuit
Blé dur précuit cuisiné, en sachet micro-ondable
Blé dur précuit, grains entiers, cuisiné, à poêler
Ravioli à la viande, sauce tomate, appertisé
Lasagnes ou cannelloni à la viande (bolognaise)
Pâtes à la bolognaise (spaghetti, tagliatelles…)
Riz cantonais
Ravioli chinois à la vapeur à la crevette, cuit
Gratin de pâtes
Lasagnes ou cannelloni aux légumes
Pâtes à la carbonara (spaghetti, tagliatelles…)
Lasagnes ou cannelloni au poisson
Pâtes fraîches farcies (ex : raviolis, ravioles du Dauphiné), au fromage, cuites
Couscous de légumes
Pâtes fraîches farcies (ex : raviolis), au fromage et aux légumes, cuites
Pâtes fraîches farcies (ex : raviolis), à la viande (ex : bolognaise), crues
Pâtes fraîches farcies (ex : raviolis), à la viande (ex : bolognaise), cuites
Pâtes fraîches farcies (ex : raviolis, ravioles du Dauphiné), au fromage, crues
Pâtes fraîches farcies (ex : raviolis), au fromage et aux légumes, crues
Nouilles sautées/poêlées aux crevettes
Riz blanc, cuit, avec poulet
Riz blanc, cuit, avec légumes et viande
Risotto, aux légumes
Risotto, aux fruits de mer
Risotto, aux fromages
Raviolis aux légumes, sauce tomate, appertisés
Pâtes fraîches farcies (ex : raviolis), aux légumes, cuites
Pâtes en sauce aux fromages (spaghetti, tagliatelles…)
Pâtes fraîches farcies (ex : ravioli), aux légumes, crues
Pâtes fraîches farcies (ex : raviolis), cuites (aliment moyen)
Feuille de vigne farcie au riz ou dolmas, égouttée
Lasagnes ou cannelloni aux légumes, cuits
Lasagnes ou canelloni aux légumes et au fromage de chèvre, cuits
Lasagnes ou cannelloni au fromage et aux épinards
Aligot (purée de pomme de terre à la tomme fraîche)
Soufflé au fromage
Tartiflette
Gougère
Fondue savoyarde (fromages, vin, pain)
Fromage pané au jambon
Pizza au fromage ou Pizza margherita
Quiche lorraine
Crêpe ou Galette fourrée béchamel jambon
Crêpe ou Galette fourrée béchamel jambon fromage
Crêpe ou Galette fourrée béchamel champignon
Tarte aux légumes
Pizza jambon fromage
Tarte au fromage
Tarte à la provençale
Pizza à la viande, type bolognaise
Burritos
Fajitas
Pizza au chorizo ou salami
Pizza aux fruits de mer
Pizza au saumon
Pizza au chèvre et lardons
Pizza aux légumes ou Pizza 4 saisons
Pizza champignons fromage
Pizza 4 fromages
Pizza (aliment moyen)
Pissaladière
Tarte à l oignon
Pizza jambon fromage champignons ou pizza royale, reine ou regina
Crêpe ou Galette fourrée béchamel fromage
Flammenkueche ou Tarte flambée aux lardons
Crêpe ou Galette fourrée béchamel jambon fromage champignon
Tarte ou Tourte aux poireaux
Tarte au saumon
Tourte au riesling
Tarte à la tomate
Crêpe ou Galette complète (œuf, jambon, fromage)
Tarte aux noix de Saint-Jacques
Pastilla au poulet
Pizza aux lardons, oignons et fromage
Crêpe ou Galette aux noix de St Jacques
Tarte ou quiche salée (aliment moyen)
Crêpe ou Galette fourrée béchamel champignon, cuite
Tarte au maroilles ou Flamiche au maroilles
Crêpe ou Galette fourrée au poisson et / ou fruits de mer
Ficelle picarde
Tarte épinard chèvre
Tielle sétoise
Pizza au thon
Pizza kebab
Pizza au poulet
Pizza type raclette ou tartiflette
Pizza au speck ou jambon cru
Focaccia, garnie
Fougasse, garnie
Croque-monsieur
Hot-dog
Hamburger, provenant de fast food
Cheeseburger, provenant de fast food
Double cheeseburger, provenant de fast food
Burger au poisson
Sandwich grec ou Kebab, pita, crudités
Sandwich grec ou Kebab, baguette, crudités
Sandwich baguette, thon, crudités (tomate, salade), mayonnaise
Sandwich panini, jambon cru, mozzarella, tomates
Sandwich baguette, jambon, oeuf dur, crudités (tomate, salade), beurre
Sandwich baguette, poulet, crudités (tomate, salade), mayonnaise
Sandwich baguette, jambon emmental
Sandwich baguette, saumon fumé, beurre
Sandwich baguette, thon, maïs, crudités
Burger au poulet
Pan bagnat
Sandwich baguette, jambon, beurre
Sandwich baguette, camembert, beurre
Sandwich baguette, pâté, cornichons
Sandwich baguette, saucisson, beurre
Sandwich baguette, jambon, emmental, beurre
Sandwich (aliment moyen)
Toasts ou Canapés salés, garnitures diverses
Sandwich baguette, crudités diverses, mayonnaise
Sandwich baguette, dinde, crudités (tomate, salade), mayonnaise
Sandwich baguette, oeuf, crudités (tomate, salade), mayonnaise
Sandwich baguette, porc, crudités (tomate, salade), mayonnaise
Sandwich baguette, merguez, ketchup moutarde
Sandwich baguette, salami, beurre
Croque-madame
Sandwich baguette (aliment moyen)
Sandwich pain de mie, garnitures diverses
Croque-monsieur, rayon frais
Sandwich pain de mie complet, jambon, crudités, fromage optionnel
Sandwich pain de mie complet, thon, crudités, mayonnaise
Sandwich pain de mie complet, jambon, fromage
Sandwich pain de mie complet, poulet, crudités, mayonnaise
Cake salé (garniture : fromage, légumes, viande, poisson, volaille, etc.)
Samossas ou Samoussas
Feuilleté au poisson et / ou fruits de mer
Spécialité chinoise ou bouchées à la vapeur, cuite
Feuilleté aux escargots
Feuilleté ou Friand au fromage
Feuilleté ou Friand à la viande
Bouchée à la reine, à la viande/volaille/quenelle
Croissant au jambon
Rouleau de printemps
Nem ou Pâté impérial
Brick garni (garniture : crevettes, légumes, volaille, viande, poisson, etc.)
Bouchée à la reine, au poisson et fruits de mer
Feuilleté ou Friand jambon fromage
Beignet de viande, volaille ou poisson, fait maison
Brick à l oeuf
Brick au boeuf
Brick à la pomme de terre
Feuilleté salé (aliment moyen)
Bouchée à la reine, garnie (aliment moyen)
Nem ou Pâté impérial, au poulet, cuit
Nem ou Pâté impérial, au porc, cuit
Nem ou Pâté impérial, aux crevettes et/ou au crabe, cuit
Croissant au jambon fromage
Avocat, pulpe, cru
Bette ou blette, crue
Carotte, crue
Champignon, tout type, cru
Salade ou chicorée frisée, crue
Chou rouge, cru
Chou-fleur, cru
Concombre, pulpe et peau, cru
Courgette, pulpe et peau, crue
Cresson de fontaine, cru
Céleri branche, cru
Endive, crue
Fenouil, cru
Laitue, crue
Oignon, cru
Pissenlit, cru
Poireau, cru
Poivron, vert, jaune ou rouge, cru
Potiron, cru
Radis rouge, cru
Tomate, crue
Artichaut, cru
Aubergine, crue
Cardon, cru
Céleri-rave, cru
Champignon de Paris ou champignon de couche, cru
Brocoli, cru
Chou de Bruxelles, cru
Épinard, cru
Haricot vert, cru
Navet, pelé, cru
Chou-rave, cru
Chou vert, cru
Haricot vert, surgelé, cru
Petits pois, crus
Asperge, pelée, crue
Chou-fleur, surgelé, cru
Épinard, surgelé, cru
Petits pois, surgelés, crus
Poivron vert, cru
Poivron rouge, cru
Radis noir, cru
Scarole, crue
Betterave rouge, crue
Échalote, crue
Mâche, crue
Légumes, mélange surgelé, crus
Champignon, chanterelle ou girolle, crue
Champignon, morille, crue
Maïs doux, en épis, surgelé, cru
Oseille, crue
Champignon, pleurote, crue
Chou blanc, cru
Tomate verte, crue
Haricot de Lima, cru
Courge musquée, pulpe, crue
Potimarron, pulpe, cru
Courge hokkaïdo, pulpe, crue
Courge melonnette, pulpe, crue
Courge doubeurre (butternut), pulpe, crue
Courge, crue
Courge spaghetti, pulpe, crue
Piment, cru
Champignon, oronge vraie, crue
Champignon, cèpe, cru
Champignon, rosé des prés, cru
Chicorée rouge, crue
Chicorée verte, crue
Citrouille, pulpe, crue
Chou chinois ou pak-choi ou pé-tsai, cru
Poivron jaune, cru
Laitue romaine, crue
Tomate cerise, crue
Pois mange-tout ou pois gourmand, cru
Panais, cru
Haricot mungo germé ou pousse de "soja", cru
Haricot beurre, cru
Salsifis noir, cru
Bambou, pousse, crue
Cresson alénois, cru
Laitue iceberg, crue
Rutabaga, cru
Haricot beurre, surgelé, cru
Brocoli, surgelé, cru
Chou de Bruxelles, surgelé, cru
Carotte, surgelée, crue
Concombre, pulpe, cru
Petits pois et carottes, surgelés, crus
Roquette, crue
Chou frisé, cru
Champignon de Paris ou champignon de couche, surgelé, cru
Courgette, pulpe et peau, surgelée, crue
Crosne, surgelé, cru
Artichaut, fond, surgelé, cru
Maïs doux, surgelé, cru
Navet, surgelé, cru
Oignon, surgelé, cru
Poireau, surgelé, cru
Salsifis, surgelé, cru
Légumes pour potages, surgelés, crus
Julienne ou brunoise de légumes, surgelée, crue
Légumes pour ratatouille, surgelés
Printanière de légumes, surgelée, crue (haricots verts, carottes, pomme de terre, petits pois, oignons)
Haricot plat, cru
Épinard, jeunes pousses pour salades, cru
Mesclun ou salade, mélange de jeunes pousses
Asperge, verte, crue
Chou romanesco ou brocoli à pomme, cru
Asperge, blanche ou violette, pelée, crue
Salicorne (Salicornia sp.), fraîche
Légumes pour couscous, surgelés, crus
Salade verte, crue, sans assaisonnement
Banane plantain, crue
Artichaut, cuit
Asperge, bouillie/cuite à l eau
Aubergine, cuite
Betterave rouge, cuite
Bette ou blette, cuite
Brocoli, cuit
Carotte, appertisée, égouttée
Carotte, cuite
Champignon, tout type, appertisé, égoutté
Chou de Bruxelles, cuit
Chou vert, cuit
Chou-fleur, cuit
Coeur de palmier, appertisé, égoutté
Courgette, pulpe et peau, cuite
Céleri branche, cuit
Céleri-rave, cuit
Épinard, cuit
Haricot mungo germé ou pousse de "soja", appertisé, égoutté
Haricot vert, cuit
Navet, cuit
Oignon, cuit
Petits pois, appertisés, égouttés
Petits pois, cuits
Poireau, cuit
Potiron, appertisé, égoutté
Salsifis, cuit
Tomate, pelée, appertisée, égouttée
Maïs doux, en épis, cuit
Macédoine de légumes, appertisée, égouttée
Épinard, appertisé, égoutté
Haricot vert, appertisé, égoutté
Haricot beurre, appertisé, égoutté
Maïs doux, appertisé, égoutté
Artichaut, appertisé, égoutté
Tomate, concentré, appertisé
Haricot vert, surgelé, cuit
Asperge, appertisée, égouttée
Chou de Bruxelles, appertisé, égoutté
Céleri branche, appertisé, égoutté
Champignon de Paris ou champignon de couche, appertisé, égoutté
Salsifis, appertisé, égoutté
Poivron vert, cuit
Poivron rouge, cuit
Petits pois et carottes, appertisés, égouttés
Chou-rave, bouilli/cuit à l eau
Chou rouge, bouilli/cuit à l eau
Potiron, cuit
Champignon de Paris ou champignon de couche, bouilli/cuit à l eau
Fenouil, bouilli/cuit à l eau
Épinard, surgelé, cuit
Chou-fleur, surgelé, cuit
Petits pois, surgelés, cuits
Champignon de Paris ou champignon de couche, sauté/poêlé, sans matière grasse
Panais, cuit
Fondue de poireau
Courge spaghetti, pulpe, cuite
Artichaut, coeur, appertisé, égoutté
Artichaut, fond, appertisé, égoutté
Tétragone cornue, cuite
Rutabaga, cuit
Tomate, pulpe, appertisée
Tomate, purée, appertisée
Bambou, pousses, appertisées, égouttées
Brocoli, surgelé, cuit
Chou de Bruxelles, surgelé, cuit
Carotte, surgelée, cuite
Champignon, lentin comestible ou shiitaké, cuit
Petits pois et carottes, surgelés, cuits
Pois mange-tout ou pois gourmand, bouilli/cuit à l eau
Chou frisé, cuit
Chou chinois (pak-choi ou pé-tsai), cuit
Cardon, cuit
Tomate, pulpe et peau, bouillie/cuite à l eau
Pois mange-tout ou pois gourmands, cuits
Échalote, cuite
Haricots verts, purée
Légumes (3-4 sortes en mélange), purée
Brocoli, purée
Tomate, coulis, appertisé (purée de tomates mi-réduite à 11%)
Carotte, purée
Courgette, purée
Tomate, double concentré, appertisé
Macédoine de légumes, surgelée
Poivron rouge, appertisé, égoutté
Céleri-rave, purée
Petits pois, purée
Épinard, purée
Tomate, pulpe et peau, rôtie/cuite au four
Légumes pour couscous, cuits
Légume cuit (aliment moyen)
Gombo, fruit, cuit
Oignon, séché
Tomate, séchée
Champignon, lentin comestible ou shiitaké, séché
Tomate, séchée, à l huile
Tapioca ou Perles du Japon, cru
Pomme de terre, sans peau, cuite au four
Pomme de terre, bouillie/cuite à l eau
Chips de pommes de terre, standard
Pomme de terre, sans peau, crue
Pomme de terre, appertisée, égouttée
Pomme de terre noisette, surgelée, crue
Pomme de terre vapeur, sous vide
Pomme de terre, sautée/poêlée
Pomme de terre, flocons déshydratés, au lait ou à la crème
Pomme de terre, purée à base de flocons, reconstituée avec lait entier, matière grasse
Pomme de terre, purée, avec lait et beurre, non salée
Pomme de terre, purée à base de flocons, reconstituée avec lait demi-écrémé et eau, non salée
Pomme de terre dauphine, surgelée, crue
Pomme de terre dauphine, surgelée, cuite
Pomme de terre, flocons déshydratés, nature
Pomme de terre nouvelle, crue
Potatoes ou Wedges ou Quartiers de pommes de terre épicés, surgelées, cuites
Pomme de terre, rôtie/cuite au four
Pomme de terre rissolée, surgelée, cuite
Pomme de terre de conservation, sans peau, bouillie/cuite à l eau
Pomme de terre primeur, sans peau, bouillie/cuite à l eau
Frites de pommes de terre, surgelées, rôties/cuites au four
Frites de pommes de terre, surgelées, cuites en friteuse
Pomme de terre duchesse, surgelée, cuite
Pomme de terre noisette, surgelée, cuite
Pomme de terre sautée/poêlée à la graisse de canard
Chips de pommes de terre, à l ancienne
Chips de pommes de terre et assimilés, allégées en matière grasse
Rostis ou Galette de pomme de terre
Pomme de terre duchesse, surgelée, crue
Pomme de terre rissolée, surgelée, crue
Frites de pommes de terre, surgelées, pour cuisson rôtie/ au four
Frites de pommes de terre, surgelées, pour cuisson micro-ondes
Frites de pommes de terre, surgelées, pour cuisson en friteuse
Pomme de terre, purée (aliment moyen)
Pomme de terre, cuite (aliment moyen)
Patate douce, crue
Patate douce, cuite
Patate douce, purée, cuisinée à la crème
Topinambour, cuit
Topinambour, cru
Banane plantain, cuite
Taro, tubercule, cru
Taro, tubercule, cuit
Igname, épluchée, crue
Igname, épluchée, bouillie/cuite à l eau
Manioc, racine crue
Manioc, racine cuite
Fruit à pain, cru
Fève, cuite
Haricot blanc, cuit
Haricot rouge, cuit
Lentille, cuite
Pois cassé, cuit
Pois chiche, cuit
Haricot flageolet, appertisé, égouttés
Lentille, cuisinée, appertisée, égouttée
Haricot blanc, appertisé, égoutté
Haricot flageolet, cuit
Haricot rouge, appertisé, égoutté
Haricot mungo, cuit
Pois chiche, appertisé, égoutté
Haricot flageolet, vert, cuit
Fève, pelée, surgelée, cuite à l eau
Fève, surgelée, cuite à l eau
Lentille verte, cuite
Lentille blonde, cuite
Lentille corail, cuite
Légume sec, cuit (aliment moyen)
Fève à écosser, fraîche
Lentille, germée
Lupin, graine crue
Fève, fraîche, surgelée
Haricot flageolet, surgelé
Fève, pelée, surgelée, crue
Haricot blanc, sec
Lentille, sèche
Pois cassé, sec
Pois chiche, sec
Fève, sèche
Haricot rouge, sec
Haricot mungo, sec
Lentille corail, sèche
Haricot flageolet, vert, sec
Lentille verte, sèche
Lentille blonde, sèche
Abricot, dénoyauté, cru
Ananas, pulpe, cru
Banane, pulpe, crue
Cassis, cru
Cerise, dénoyautée, crue
Citron, pulpe, cru
Coing, cru
Figue, crue
Fraise, crue
Framboise, crue
Fruit de la passion ou maracudja, pulpe et pépins, cru
Grenade, pulpe et pépins, crue
Groseille, crue
Groseille à maquereau, crue
Kiwi, pulpe et graines, cru
Litchi, pulpe, cru
Mangue, pulpe, crue
Melon cantaloup (par ex.: Charentais, de Cavaillon) pulpe, cru
Myrtille, crue
Mûre (de ronce), crue
Nectarine ou brugnon, pulpe et peau, crue
Orange, pulpe, crue
Papaye, pulpe, crue
Pastèque, pulpe, crue
Poire, pulpe et peau, crue
Pomme, pulpe et peau, crue
Pomelo (dit Pamplemousse), pulpe, cru
Prune Reine-Claude, crue
Pêche, pulpe et peau, crue
Raisin blanc, à gros grain (type Italia ou Dattier), cru
Raisin noir, cru
Rhubarbe, tige, crue
Pomme, pulpe, crue
Carambole, pulpe, crue
Figue de Barbarie, pulpe et graines, crue
Kaki, pulpe, cru
Citron vert ou Lime, pulpe, cru
Mûre noire (du mûrier), crue
Tamarin, fruit immature, pulpe, cru
Clémentine, pulpe, crue
Goyave, pulpe, crue
Pomme Canada, pulpe, crue
Mandarine, pulpe, crue
Prune, crue
Poire, pulpe, crue
Griotte, crue
Raisin, cru
Canneberge ou cranberry, crue
Citron, zeste, cru
Sureau, baie, crue
Myrtille, surgelée, crue
Salade de fruits, crue
Framboise, surgelée, crue
Mûre (de ronce), surgelée, crue
Pomelo (dit Pamplemousse) jaune, pulpe, cru
Pomelo (dit Pamplemousse) rose, pulpe, cru
Kumquat, sans pépin, cru
Pamplemousse chinois, pulpe, cru
Pomme Golden, pulpe et peau, crue
Melon miel ou melon honeydew, pulpe, cru
Fruits rouges, crus (framboises, fraises, groseilles, cassis)
Compote de pomme
Rhubarbe, tige, cuite, sucrée
Compote, tout type de fruits
Compote, tout type de fruits, allégée en sucres
Compote, tout type de fruits, allégée en sucres, rayon frais
Dessert de fruits, tout type de fruits (en taux de sucres : compotes allégées en sucres < desserts de fruits < compotes allégée)
Purée de fruits, tout type de fruits, type "compote sans sucres ajoutés"
Pomme, pulpe, rôtie/cuite au four
Pomme, pulpe, bouillie/cuite à l eau
Macédoine ou cocktail ou salade de fruits, au sirop, appertisé, égoutté
Macédoine ou cocktail ou salade de fruits, au sirop, appertisé, non égoutté
Macédoine ou cocktail ou salade de fruits, au sirop léger, appertisé, égoutté
Macédoine ou cocktail ou salade de fruits, au sirop léger, appertisé, non égoutté
Abricot au sirop léger, appertisé, égoutté
Abricot au sirop léger, appertisé, non égoutté
Abricot au sirop, appertisé, égoutté
Abricot au sirop, appertisé, non égoutté
Ananas au sirop et jus d ananas, appertisé, égoutté
Ananas au sirop et jus d ananas, appertisé, non égoutté
Ananas au sirop léger, appertisé, égoutté
Ananas au sirop léger, appertisé, non égoutté
Pêche au sirop léger, appertisée, égouttée
Pêche au sirop léger, appertisée, non égouttée
Poire au sirop léger, appertisée, non égouttée
Abricot, dénoyauté, sec
Datte, pulpe et peau, sèche
Figue, sèche
Pruneau, sec
Raisin, sec
Banane, pulpe, sèche
Pomme, sèche
Pêche, sèche
Farine de châtaigne
Amande (avec peau)
Cacahuète ou Arachide
Cacahuète, grillée, salée
Noisette
Noix, séchée, cerneaux
Noix de coco, amande mûre, fraîche
Noix de coco, amande, sèche
Noix du Brésil
Pistache, grillée, salée
Sésame, graine
Tournesol, graine
Crème de marrons
Noix de coco, amande immature, fraîche
Crème de marrons vanillée, appertisée
Mélange apéritif de graines salées et raisins secs
Noix de cajou, grillée, salée
Châtaigne, bouillie/cuite à l eau
Châtaigne, grillée
Noix, fraîche
Châtaigne, crue
Pignon de pin
Noix de pécan
Noix de macadamia
Cucurbitacées, graine
Luzerne, graine germée
Luzerne, graine
Noisette grillée
Lin, graine
Sésame, graine décortiquée
Cacahuète, grillée
Sésame, grillé, graine décortiquée
Châtaigne ou Marron, appertisé
Amande, mondée, émondée ou blanchie
Amande, grillée, salée
Noix de macadamia, grillée, salée
Pistache, grillée
Tournesol, graine, grillé, salé
Noix de pécan, salées
Chia, graine, séchée
Mélange apéritif de graines (non salées) et fruits séchés
Mélange apéritif de graines (non salées) et raisins secs
Noisette grillée, salée
Lin, brun, graine
Pâte d amande, préemballée
Beurre de cacahuète ou Pâte d arachide
Tahin ou Purée de sésame
Arachide, bouillie/cuite à l eau, salée
Soja, graine entière
Blé dur précuit, grains entiers, cuit, non salé
Nouilles asiatiques cuites, aromatisées
Nouilles asiatiques cuites, nature, non salées
Riz complet, cuit, non salé
Riz blanc, cuit, non salé
Riz blanc étuvé, cuit, non salé
Riz rouge, cuit, non salé
Riz sauvage, cuit, non salé
Orge perlée, bouilli/cuite à l eau, non salée
Mil, cuit, non salé
Quinoa, bouilli/cuit à l eau, non salé
Polenta ou semoule de maïs, cuite, non salée
Couscous (semoule de blé dur roulée précuite à la vapeur), cuite, non salée
Boulgour de blé, cuit, non salé
Pâtes sèches standard, cuites, non salées
Pâtes fraîches, aux œufs, cuites, non salées
Pâtes sèches, aux œufs, cuites, non salées
Pâtes sèches, sans gluten, cuites, non salées
Pâtes sèches, au blé complet, cuites, non salées
Vermicelle de riz, cuite, non salée
Vermicelle de soja, cuite, non salée
Gnocchi à la semoule, cuit
Gnocchi à la pomme de terre, cuit
Gnocchi, cuit (aliment moyen)
Frik (blé dur immature concassé), cuit, non salé
Épeautre, cru
Blé de Khorasan, cru
Blé tendre entier ou froment, cru
Blé germé, cru
Blé dur entier, cru
Blé dur précuit, entier, cru
Riz blanc, cru
Riz blanc étuvé, cru
Riz complet, cru
Riz sauvage, cru
Riz rouge, cru
Riz thaï ou basmati, cru
Riz, mélange de variétés (blanc, complet, rouge, sauvage, etc.), cru
Maïs entier, cru
Avoine, crue
Orge entière, crue
Orge perlée, crue
Mil entier, cru
Quinoa, cru
Amarante, crue
Sorgho entier, cru
Sarrasin entier, cru
Seigle entier, cru
Semoule de blé dur, crue
Mélange de céréales et légumineuses, cru
Polenta ou semoule de maïs, précuite, sèche
Couscous (semoule de blé dur roulée précuite à la vapeur), crue
Boulgour de blé, cru
Pâtes sèches standard, crues
Pâtes fraîches, aux œufs, crues
Pâtes sèches, aux œufs, crues
Nouilles asiatiques aromatisées, déshydratées
Pâtes sèches, au blé complet, crues
Pâtes sèches, sans gluten, crues
Vermicelle de riz, sèche
Vermicelle de soja, sèche
Gnocchi à la pomme de terre, cru
Gnocchi à la semoule, cru
Frik (blé dur immature concassé), cru
Pain (aliment moyen)
Pain, baguette, courante
Pain, baguette ou boule, au levain
Pain, baguette, de tradition française
Pain, baguette ou boule, bis (à la farine T80 ou T110) 
Pain courant français, 400g ou boule
Pain, baguette ou boule, bio (à la farine T55 jusqu à T110)
Pain, baguette ou boule, de campagne
Pain complet ou intégral (à la farine T150)
Pain de mie, complet
Pain de mie, au son
Pain de mie, multicéréale
Pain au son
Pain de seigle, et froment
Pain, sans gluten
Pain, baguette, sans sel
Pain panini
Pain pita
Pain de mie, courant
Pain de mie, sans croûte, préemballé
Pain de mie brioché, préemballé
Pain, baguette ou boule, aux céréales et graines, artisanal
Muffin anglais, complet, petit pain spécial, préemballé
Muffin anglais, petit pain spécial, préemballé
Bagel
Pain pour hamburger ou hot dog (bun), préemballé
Pain blanc maison (avec farine pour machine à pain)
Pain de campagne maison (avec farine pour machine à pain)
Pain pour hamburger ou hot dog (bun), complet, préemballé
Tortilla souple (à garnir), à base de maïs
Tortilla souple (à garnir), à base de blé
Blini
Pain grillé, domestique
Biscotte classique
Biscotte briochée
Biscotte sans adjonction de sel
Biscotte multicéréale
Biscotte complète ou riche en fibres
Crackers de table au froment
Galette de riz soufflé complet
Galettes multicéréales soufflées
Pain grillé, tranches, au froment
Pain grillé brioché, tranché, préemballé
Pain grillé suédois au froment
Pain grillé suédois aux graines de lin
Tartine craquante, extrudée et grillée
Pain grillé suédois au blé complet
Pain grillé suédois aux fruits
Pain grillé, tranches, multicéréale
Croûtons
Croûtons à tartiner
Croûtons nature, préemballés
Chapelure
Gressins
Croûton à l ail aux fines herbes ou aux oignons, préemballé
Pain brioché ou viennois
Viennoiserie (aliment moyen)
Croissant, sans précision
Croissant ordinaire, artisanal
Croissant au beurre, artisanal
Croissant aux amandes, artisanal
Pain au lait, artisanal
Pain au lait, préemballé
Pain au lait aux pépites de chocolat, préemballé
Pain aux raisins (viennoiserie)
Pain au chocolat feuilleté, artisanal
Pain au chocolat, préemballé
Brioche aux pépites de chocolat
Brioche fourrée au chocolat
Brioche fourrée aux fruits
Brioche fourrée crème pâtissière (type "chinois"), préemballée
Brioche, préemballée
Brioche, sans précision
Brioche, de boulangerie traditionnelle
Couronne de Noël (Brioche) aux fruits confits, préemballée
Brioche pur beurre
Chouquette
Chausson aux pommes
Flocons d avoine, bouillis/cuits à l eau
Grains de blé soufflés au miel ou caramel, enrichis en vitamines et minéraux
Céréales pour petit déjeuner chocolatées, non fourrées, enrichies en vitamines et minéraux
Céréales pour petit déjeuner riches en fibres, avec ou sans fruits, enrichies en vitamines et minéraux
Céréales pour petit déjeuner (aliment moyen)
Muesli (aliment moyen)
Pétales de maïs natures, enrichis en vitamines et minéraux
Riz soufflé nature, enrichi en vitamines et minéraux
Céréales pour petit déjeuner riches en fibres, au chocolat, enrichies en vitamines et minéraux
Pétales de blé chocolatés, enrichis en vitamines et minéraux
Pétales de blé chocolatés (non enrichis en vitamines et minéraux)
Riz soufflé chocolaté (non enrichi en vitamines et minéraux)
Céréales chocolatées pour petit déjeuner, non fourrées, (non enrichies en vitamines et minéraux)
Pétales de maïs natures (non enrichis en vitamines et minéraux)
Céréales pour petit déjeuner fourrées au chocolat ou chocolat-noisettes, enrichies en vitamines et minéraux
Céréales pour petit déjeuner fourrées au chocolat ou chocolat-noisettes
Céréales pour petit déjeuner fourrées, fourrage autre que chocolat, enrichies en vitamines et minéraux
Céréales pour petit déjeuner "équilibre" nature ou au miel, enrichies en vitamines et minéraux
Céréales pour petit déjeuner "équilibre" au chocolat, enrichies en vitamines et minéraux
Céréales pour petit déjeuner "équilibre" aux fruits, enrichies en vitamines et minéraux
Céréales pour petit déjeuner "équilibre" aux fruits secs (à coque), enrichis en vitamines et minéraux
Céréales pour petit déjeuner "équilibre" au chocolat (non enrichies en vitamines et minéraux)
Céréales pour petit déjeuner "équilibre" aux fruits (non enrichies en vitamines et minéraux)
Céréales pour petit déjeuner "équilibre" nature (non enrichies en vitamines et minéraux)
Céréales pour petit déjeuner, enrichies en vitamines et minéraux (aliment moyen)
Céréales pour petit déjeuner, non enrichies en vitamines et minéraux (aliment moyen)
Pétales de maïs glacés au sucre (non enrichis en vitamines et minéraux)
Muesli croustillant aux fruits et/ou fruits secs, graines (non enrichi en vitamines et minéraux)
Muesli croustillant au chocolat (non enrichi en vitamines et minéraux)
Muesli floconneux aux fruits ou fruits secs, enrichi en vitamines et minéraux
Muesli croustillant aux fruits ou fruits secs, enrichi en vitamines et minéraux
Muesli croustillant au chocolat, avec ou sans fruits, enrichi en vitamines et minéraux
Muesli floconneux aux fruits ou fruits secs, sans sucres ajoutés
Grains de blé soufflés chocolatés, enrichis en vitamines et minéraux
Céréales pour petit déjeuner très riches en fibres, enrichies en vitamines et minéraux
Pétales de maïs glacés au sucre, enrichis en vitamines et minéraux
Pétales de blé avec noix, noisettes ou amandes, enrichis en vitamines et minéraux
Muesli floconneux ou de type traditionnel
Boules de maïs soufflées au miel (non enrichies en vitamines et minéraux)
Blé khorasan complet soufflé
Riz soufflé chocolaté, enrichi en vitamines et minéraux
Boules de maïs soufflées au miel, enrichies en vitamines et minéraux
Céréales complètes soufflées, enrichies en vitamines et minéraux
Multi-céréales soufflées ou extrudées, enrichies en vitamines et minéraux
Muesli floconneux aux fruits ou fruits secs (non enrichi en vitamines et minéraux)
Muesli enrichi en vitamines et minéraux (aliment moyen)
Flocon d avoine précuit
Muesli non enrichi en vitamines et minéraux (aliment moyen)
Barre céréalière pour petit déjeuner au lait, chocolatée ou non, enrichie en vitamines et minéraux
Barre céréalière "équilibre" aux fruits, enrichie en vitamines et minéraux
Barre céréalière "équilibre" chocolatée, enrichie en vitamines et minéraux
Barre céréalière diététique hypocalorique
Barre céréalière chocolatée
Barre céréalière aux fruits
Barre céréalière aux amandes ou noisettes
Tartine craquante, extrudée et grillée, fourrée au chocolat
Tartine craquante, extrudée et grillée, fourrée aux fruits
Macaron sec
Biscuit sec, sans précision
Biscuit sec nature
Biscuit sec à teneur garantie en vitamines
Biscuit sec à teneur garantie en vitamines et minéraux
Biscuit sec aux fruits, hyposodé
Biscuit sec croquant au chocolat, allégé en matière grasse
Biscuit sec fourré aux fruits, allégé en matière grasse
Spéculoos
Biscuit sec, avec matière grasse végétale
Biscuit sec, petits fours en assortiment
Biscuit sec petit beurre
Biscuit sec avec tablette de chocolat
Biscuit sec petit beurre au chocolat
Biscuit sec au lait
Biscuit sec croquant (ex : tuile) sans chocolat, allégé en matière grasse
Biscuit sec pour petit déjeuner
Biscuit sec pour petit déjeuner, allégé en sucres
Biscuit sec chocolaté, préemballé
Biscuit sec fourré à la pâte ou purée de fruits
Biscuit sec avec nappage chocolat
Barre biscuitée fourrée aux fruits, allégée en matière grasse
Biscuit sec pour petit déjeuner, au chocolat
Biscuit aux céréales pour petit déjeuner, enrichis en vitamines et minéraux
Biscuit sec au beurre, sablé, galette ou palet
Biscuit sec au beurre, sablé, galette ou palet, au chocolat
Biscuit sec chocolaté, type barquette
Biscuit sec chocolaté, type tartelette
Biscuit sec chocolaté, type galette
Biscuit sec, sablé, galette ou palet, aux fruits
Biscuit sec fourré fruits à coque (non ou légèrement chocolaté)
Florentin (biscuit sec sucré chocolaté aux amandes)
Biscuit pâtissier meringué
Sablé à la noix de coco
Sablé pâtissier
Sablé aux fruits (pomme, fruits rouges, etc.)
Sablé au cacao ou chocolat, au praliné ou autre
Goûter sec fourré ("sandwiché") parfum lait ou vanille
Goûter sec fourré ("sandwiché") parfum chocolat
Goûter sec fourré ("sandwiché") parfum fruits
Gaufrette ou éventail sans fourrage
Gaufrette fourrée chocolat, préemballée
Gaufrette fourrée fruits à coque (noisette, amande, praline, etc.), chocolatée ou non, préemballée
Gaufrette, fourrée vanille, préemballée
Gaufrette fourrée, aux fruits
Cigarette
Crêpe dentelle
Crêpe dentelle au chocolat, préemballée
Biscuit sec aux œufs à la cuillère (cuiller) ou Boudoir
Biscuit sec type langue de chat ou cigarette russe
Meringue
Biscuit sec ou tuile, aux amandes
Biscuit sec type tuile, aux fruits
Biscuit sec feuilleté, type palmier ou autres
Palmier, artisanal
Biscuit sec (génoise) nappage aux fruits, type barquette
Biscuit sec nappé aux fruits, tartelette
Biscuit moelleux fourré à l orange et enrobé de sucre glace
Cookie aux pépites de chocolat
Cône ou cornet classique, pour glace
Génoise sèche fourrée aux fruits et nappée de chocolat
Biscuit sec pauvre en glucides
Pop-corn ou Maïs éclaté, à l huile, salé
Pop-corn ou Maïs éclaté, à l air, non salé
Pop-corn ou Maïs éclaté, au caramel
Chips de crevette
Chips de maïs ou tortilla chips
Biscuit apéritif soufflé, à base de pomme de terre
Biscuit apéritif, mini bretzel ou sticks
Biscuit apéritif soufflé, à base de pomme de terre et de soja
Biscuit apéritif (aliment moyen)
Biscuit apéritif soufflé, à base de maïs, sans cacahuète
Biscuit apéritif, crackers, garni ou fourré, au fromage
Biscuit apéritif, crackers, nature
Biscuit apéritif, crackers, nature, allégé en matière grasse
Biscuit apéritif soufflé, à base de maïs, à la cacahuète
Biscuit apéritif à base de pomme de terre, type tuile salée
Cacahuètes (arachide) enrobées d un biscuit, pour apéritif
Biscuit apéritif feuilleté
Crêpe dentelle (pour apéritif) au fromage, préemballée
Gâteau (aliment moyen)
Gâteau Paris-Brest (pâte à choux crème mousseline praliné)
Gâteau au chocolat type forêt noire (génoise au chocolat et crème multi-couches, avec ou sans cerises)
Gâteau mousse de fruits sur génoise, type miroir, bavarois
Entremets type Opéra
Fraisier ou framboisier
Baba au rhum, préemballé
Canelé
Macaron moelleux fourré à la confiture ou à la crème
Bûche de Noël pâtissière
Brownie au chocolat
Rocher coco ou Congolais (petit gâteau à la noix de coco)
Biscuit de Savoie
Quatre-quarts ou barre pâtissière, préemballé
Gâteau au citron, tout type
Far aux pruneaux
Kouign Amann
Pain d épices
Baklava ou Baklawa (pâtisserie orientale aux amandes et sirop)
Corne de gazelle (pâtisserie orientale aux amandes et sirop)
Chou à la crème (chantilly ou pâtissière)
Chou à la crème chantilly
Chou à la crème pâtissière
Éclair
Tarte aux fruits et crème pâtissière
Tarte normande aux pommes (garniture farine, œufs, crème, sucre, calvados)
Tarte au citron
Tarte ou tartelette aux pommes
Tarte aux fraises
Crumble aux pommes
Tarte aux abricots
Tarte aux fruits rouges
Tarte Tatin aux pommes
Tarte au chocolat, fabrication artisanale
Tarte ou tartelette aux fruits
Flan pâtissier aux oeufs ou à la parisienne
Charlotte aux fruits
Gâteau au chocolat
Gâteau moelleux au chocolat, préemballé
Gâteau au yaourt
Gâteau au fromage blanc
Gâteau moelleux nature type génoise
Galette des rois feuilletée
Galette des rois feuilletée, fourrée frangipane, et Pithiviers
Crêpe, nature, préemballée, rayon frais
Crêpe, nature, préemballée, rayon température ambiante
Galette de sarrasin, nature, préemballée
Gâteau basque, crème pâtissière
Gâteau basque, cerises
Crêpe préemballée, fourrée au sucre
Crêpe maison, fourrée à la confiture
Crêpe maison, fourrée au chocolat ou à la pâte à tartiner chocolat et noisettes
Crêpe préemballée, fourrée chocolat
Crêpe préemballée, fourrée fraise
Gaufre moelleuse (type bruxelloise ou liégeoise), nature ou sucrée, préemballée
Gaufre moelleuse (type bruxelloise ou liégeoise), chocolatée, préemballée
Gaufre croustillante (fine ou sèche), nature ou sucrée, préemballée
Gaufre croustillante (fine ou sèche), chocolatée, préemballée
Beignet rond moelleux, sans fourrage, saupoudré de sucre
Beignet à la confiture
Beignet fourré aux fruits, préemballé
Beignet fourré goût chocolat, préemballé
Pâtisserie (aliment moyen)
Cake aux fruits
Gâteau marbré
Gâteau sablé aux fruits, préemballé
Gâteau moelleux aux fruits
Gâteau moelleux aux fruits à coque
Gâteau moelleux fourré au chocolat ou aux pépites de chocolat ou au lait
Génoise fourrée et nappée au chocolat
Gâteau moelleux fourré aux fruits type mini-roulé ou mini-cake fourré
Muffin, aux myrtilles ou au chocolat
Madeleine traditionnelle, pur beurre
Madeleine chocolatée, préemballée
Madeleine ordinaire, préemballée
Tarte aux poires amandine
Gâteau aux amandes type financier
Gâteau aux amandes, préemballé
Mille-feuille
Fécule de pomme de terre
Flocon d avoine
Farine de blé tendre ou froment T110
Farine de blé tendre ou froment T150
Farine de blé tendre ou froment T65
Farine de blé tendre ou froment T55 (pour pains)
Farine de blé tendre ou froment avec levure incorporée
Farine de blé tendre ou froment T45 (pour pâtisserie)
Farine de blé tendre ou froment T80
Farine d épeautre (grand épeautre)
Amidon de maïs ou fécule de maïs
Farine de riz
Farine de seigle T170
Farine de seigle T85
Farine de seigle T130
Farine de sarrasin
Farine de maïs
Farine d orge
Farine de millet
Farine de pois chiche
Farine de soja
Amidon de riz
Pâte à pizza fine, crue
Préparation pour pâte à pizza
Pâte brisée, crue
Pâte brisée, matière grasse végétale, cuite
Pâte brisée, pur beurre, crue
Pâte brisée, pur beurre, surgelée, crue
Pâte feuilletée, matière grasse végétale, crue
Pâte feuilletée, surgelée, crue
Pâte feuilletée, cuite
Pâte feuilletée pur beurre, crue
Pâte feuilletée pur beurre, surgelée crue
Pâte feuilletée pur beurre, cuite
Feuille de brick, cuite à sec sans matière grasse
Pâte sablée, crue
Pâte sablée, cuite
Pâte sablée pur beurre, crue
Pâte phyllo ou Pâte filo, crue
Pâte sablée pur beurre, surgelée, crue
Pâte sablée pur beurre, cuite
Pâte à pizza crue
Khatfa feuille de brick, préemballée
Pâte à pizza cuite
Foie gras, canard, bloc (aliment moyen)
Viande blanche, cuite (aliment moyen)
Viande rouge, cuite (aliment moyen)
Viande cuite (aliment moyen)
Volaille, cuite (aliment moyen)
Abat, cuit (aliment moyen)
Boeuf, entrecôte, partie maigre, grillée/poêlée
Boeuf, braisé
Boeuf, gîte à la noix, cuit
Boeuf, faux-filet, rôti/cuit au four
Boeuf, faux-filet, grillé/poêlé
Boeuf, plat de côtes, braisé
Boeuf, hampe, grillée/poêlée
Boeuf, joue, braisée ou bouillie
Boeuf, jarret, bouilli/cuit à l eau
Boeuf, tende de tranche, grillée/poêlée
Boeuf, tende de tranche, rôtie/cuite au four
Boeuf, steak ou bifteck, grillé
Boeuf, onglet, grillé
Boeuf, rumsteck, grillé
Boeuf, boule de macreuse, grillée/poêlée
Boeuf, rosbif, rôti/cuit au four
Boeuf, bavette d aloyau, grillée/poêlée
Boeuf, boule de macreuse, rôtie/cuite au four
Boeuf, à bourguignon ou pot-au-feu, cuit
Boeuf, collier, braisé
Bœuf, steak haché 5% MG, cuit
Bœuf, steak haché 10% MG, cuit
Bœuf, steak haché 15% MG, cuit
Bœuf, steak haché 20% MG, cuit
Bœuf, steak haché, cuit (aliment moyen)
Boeuf, paleron, braisé ou bouilli
Boeuf, queue, bouillie/cuite à l eau
Veau, côte, grillée/poêlée
Veau, carré, sauté/poêlé
Veau, escalope, cuite
Veau, noix, grillée/poêlée
Veau, noix, rôtie
Veau, filet, rôti/cuit au four
Veau, rôti, cuit
Veau, épaule, grillée/poêlée
Veau, épaule, braisée ou bouillie
Veau, viande, cuite (aliment moyen)
Veau, jarret, braisé ou bouilli
Veau, tête, bouillie/cuite à l eau
Veau, collier, braisé ou bouilli
Porc, longe, cuite
Porc, épaule, cuite
Porc, côte, grillée
Porc, rouelle de jambon, cuite
Porc, carré, cuit
Porc, filet, maigre, en rôti, cuit
Porc, filet mignon, cuit
Porc, viande, cuite (aliment moyen)
Porc, rôti, cuit
Porc, travers, braisé
Porc, échine, rôtie/cuite au four
Porc, escalope de jambon, cuite
Poulet, cuisse, viande et peau, rôtie/cuite au four
Poulet, viande et peau, rôti/cuit au four
Poulet, cuisse, viande, rôti/cuit au four
Poulet, filet, sans peau, cuit
Poulet, cuisse, viande, bouilli/cuit à l eau
Poulet, cuisse, viande et peau, bouilli/cuit à l eau
Poulet, poitrine, viande et peau, rôti/cuit au four
Poulet, aile, viande et peau, rôti/cuit au four
Dinde, viande, rôtie/cuite au four
Dinde, escalope, sautée/poêlée
Dinde, escalope, rôtie/cuite au four
Agneau, côtelette, grillée
Agneau, gigot, rôti/cuit au four
Agneau, épaule, rôtie/cuite au four
Agneau, épaule, maigre, rôtie/cuite au four
Agneau, collier, braisé ou bouilli
Agneau, côte filet, grillée/poêlée
Agneau, côte première, grillée/poêlée
Agneau, selle, partie maigre, rôtie/cuite au four
Agneau, gigot, grillé/poêlé
Agneau, gigot, braisé
Agneau, selle, partie maigre, grillée/poêlée
Agneau, côte ou côtelette, cuite (aliment moyen)
Agneau, viande, cuite (aliment moyen)
Agneau, côte ou côtelette, grillée/poêlée (aliment moyen)
Chevreuil, rôti/cuit au four
Sanglier, rôti/cuit au four
Cerf, rôti/cuit au four
Gibier à poil, cuit (aliment moyen)
Faisan, viande, rôtie/cuite au four
Gibier à plumes, viande, cuit (aliment moyen)
Cheval, viande, rôtie/cuite au four
Cheval, tende de tranche, grillée/poêlée
Cheval, faux-filet, grillé/poêlé
Cheval, entrecôte, grillée/poêlée
Cheval, faux-filet, rôti/cuit au four
Cheval, tende de tranche, rôtie/cuite au four
Chevreau, cuit
Lapin, viande braisée
Lapin, viande cuite
Lapin de garenne, viande, cuite
Chapon, viande et peau, rôti/cuit au four
Caille, viande et peau, cuite
Canard, viande et peau, rôti/cuit au four
Canard, viande, rôtie/cuite au four
Canard, magret, grillé/poêlé
Oie, viande, rôtie/cuite au four
Oie, viande et peau, rôtie/cuite au four
Pigeon, viande, rôtie/cuite au four
Autruche, viande cuite
Cervelle, agneau, cuite
Cervelle, porc, braisée
Cervelle, veau, cuite
Coeur, boeuf, cuit
Coeur, poulet, cuit
Coeur, dinde, cuit
Coeur, agneau, cuit
Foie, agneau, cuit
Foie, génisse, cuit
Foie, veau, cuit
Foie, porc, cuit
Foie, poulet, cuit
Foie, dinde, cuit
Langue, veau, cuite
Langue, boeuf, cuite
Ris, agneau, cuit
Ris, veau, braisé ou sauté/poêlé
Rognon, cuit (aliment moyen)
Rognon, boeuf, cuit
Rognon, porc, cuit
Rognon, agneau, braisé
Rognon, veau, braisé ou sauté/poêlé
Gésier, canard, confit, appertisé
Boeuf, côte, crue
Boeuf, épaule, crue
Boeuf, gîte à la noix, cru
Boeuf, entrecôte, crue
Boeuf, faux-filet, cru
Boeuf, faux-filet, label rouge, cru
Boeuf, plat de côtes, cru
Boeuf, hampe, crue
Boeuf, joue, crue
Boeuf, jarret, cru
Boeuf, tende de tranche, crue
Boeuf, steak ou bifteck, cru
Boeuf, boule de macreuse, crue
Boeuf, onglet, cru
Boeuf, rumsteck, cru
Boeuf, bavette d aloyau, crue
Boeuf, à bourguignon ou pot-au-feu, cru
Bœuf, steak haché 5% MG, cru
Bœuf, steak haché 10% MG, cru
Bœuf, steak haché 15% MG, cru
Bœuf, steak haché 20% MG, cru
Boeuf, paleron, cru
Veau, côte, crue
Veau, carré, cru
Veau, escalope, crue
Veau, noix, crue
Veau, filet, cru
Veau, steak haché 20% MG, cru
Veau, steak haché 15% MG, cru
Veau, poitrine, crue
Veau, rôti, cru
Veau, épaule, crue
Veau, pied, cru
Veau, jarret, cru
Veau, collier, cru
Porc, épaule, crue
Porc, poitrine, crue
Porc, longe, crue
Porc, jarret, cru
Porc, côte, crue
Porc, rouelle de jambon, crue
Porc, carré, cru
Porc, filet, maigre, cru
Porc, filet mignon, cru
Porc, rôti, cru
Porc, échine, crue
Porc, travers, cru
Porc, escalope de jambon, crue
Porc, bardière découennée, crue
Porc, gorge, découennée, crue
Porc, hachage sans jarret, sans bateau, découenné, dégraisssé, désossé, cru
Porc, jambon sans jarret, sans bateau, découenné, dégraisssé, désossé, cru
Porc, jambonneau arrière, découenné, dégraisssé, désossé, cru
Porc, maigre 90/10, cru
Porc, maigre 80/20, cru
Porc, palette, découennée, dégraisssée, désossée, crue
Porc, couenne, crue
Porc, poitrine cutter, sans mouille, crue
Porc, rôti filet avec chaînette, cru
Poulet, cuisse, viande et peau, cru
Poulet, viande, crue
Poulet (var. blanc), viande et peau, cru
Poulet fermier, viande et peau, cru
Poulet, viande et peau, cru
Poulet, filet, sans peau, cru
Poulet, haut de cuisse, viande, cru
Poulet éviscéré sans abats, cru
Poulet, pilon, cru
Poulet, aile, viande et peau, cru
Poulet, cuisse, viande, cru
Poulet, poitrine, viande et peau, cru
Dinde, viande et peau, crue
Dinde, viande, crue
Dinde, escalope, crue
Dinde, cuisse, viande et peau, crue
Dinde, cuisse, viande sans peau, crue
Dinde, aile, crue
Mouton, viande, crue
Mouton, épaule, crue
Mouton, pied, cru
Mouton, tête, crue
Mouton, gigot, cru
Agneau, côtelette, crue
Agneau, gigot, cru
Agneau, épaule, crue
Agneau, épaule, maigre, crue
Agneau, collier, cru
Agneau, selle, crue
Agneau, côte filet, crue
Agneau, côte première, crue
Agneau, côte ou côtelette, crue (aliment moyen)
Sanglier, cru
Lièvre, viande crue
Chevreuil, cru
Cerf, cru
Faisan, viande, cru
Faisan, viande et peau, cru
Cheval, viande, crue
Cheval, steak, cru
Cheval, tende de tranche, crue
Cheval, faux-filet, cru
Cheval, entrecôte, crue
Chevreau, cru
Lapin, viande crue
Lapin de garenne, viande, crue
Poule, viande et peau, crue
Poule, viande ,crue
Poule, cuisse, crue
Chapon, viande et peau, cru
Caille, viande et peau, crue
Caille, viande, crue
Canard, viande, crue
Canard, cuisse avec peau, sans os, crue
Canard, viande et peau, cru
Canard, magret, cru
Oie, viande crue
Oie, viande et peau, crue
Pigeon, cru
Pintade, crue
Pintade, poitrine, crue
Pintade, cuisse, crue
Autruche, viande crue
Cervelle, agneau, crue
Cervelle, porc, crue
Cervelle, veau, crue
Coeur, boeuf, cru
Coeur, poulet, cru
Coeur, dinde, cru
Coeur, agneau, cru
Coeur, porc, cru
Coeur, veau, cru
Foie, agneau, cru
Foie, génisse, cru
Foie, veau, cru
Foie, volaille, cru
Foie, lapin, cru
Foie, poulet, cru
Foie, dinde, cru
Foie, porc, cru
Foie, oie, cru
Foie, canard, cru
Langue, boeuf, crue
Langue, veau, crue
Langue, porc, crue
Ris, agneau, cru
Ris, veau, cru
Rognon, boeuf, cru
Rognon, porc, cru
Rognon, agneau, cru
Rognon, veau, cru
Tripes, boeuf, crues
Sang, boeuf, cru
Gésier, poulet, cru
Charcuterie (aliment moyen)
Jambon persillé en gelée
Jambon de porc à cuire ou Jambon à rôtir/cuire au four
Filet de bacon
Jambon cuit, fumé
Jambon cuit, supérieur
Jambon cuit, supérieur, avec couenne
Jambon cuit, supérieur, découenné
Jambon à l os braisé
Jambon cuit, supérieur, découenné dégraissé
Jambon cuit, supérieur, à teneur réduite en sel
Jambon cuit, choix
Épaule de porc, cuite, choix, découennée dégraissée
Jambon cuit, choix, avec couenne
Jambon cuit, choix, découenné dégraissé
Rond de jambon cuit
Dés, allumettes, râpé ou haché de jambon
Épaule de porc, cuite, standard, découennée dégraissée
Jambon cuit, de Paris, découenné dégraissé
Dés, allumettes, râpé ou haché de jambon de volaille
Jambonneau, cuit
Jambon de poulet ou Blanc de poulet en tranche
Jambon de dinde ou Blanc de dinde en tranche
Jambon cru
Jambon cru, fumé
Jambon sec, découenné, dégraissé
Jambon cru, fumé, allégé en matière grasse
Jambon de Bayonne
Jambon sec
Jambon sec de Parme
Jambon sec Serrano
Coppa
Pancetta ou Poitrine roulée sèche
Saucisson sec
Saucisson sec pur porc
Saucisson sec pur porc, qualité supérieure
Rosette ou Fuseau
Saucisse sèche
Saucisson sec aux noix et/ou noisettes
Chipolata, cuite
Saucisse de Toulouse, cuite
Chair à saucisse, crue
Chair à saucisse, pur porc, crue
Chair à saucisse, porc et bœuf, crue
Saucisse de Morteau
Saucisse de Montbéliard
Saucisse de Morteau, bouillie/cuite à l eau
Saucisse de Toulouse, crue
Chipolata, crue
Saucisse suisse à cuire
Saucisse alsacienne fumée ou Gendarme
Saucisse de volaille, façon charcutière
Saucisse de volaille, type Knack
Saucisse de Francfort
Merguez, crue
Merguez, pur bœuf, crue
Merguez, porc et bœuf, crue
Merguez, bœuf, mouton et porc, crue
Merguez, boeuf et mouton, cuite
Merguez, boeuf et mouton, crue
Saucisse de foie
Diot, cru
Saucisse de Strasbourg ou Knack
Saucisse cocktail
Saucisse viennoise, crue
Saucisse de jambon pur porc
Saucisse de bière
Saucisse de langue à la pistache
Saucisse (aliment moyen)
Confit de foie de porc
Confit de foie de volaille
Pâté au poivre vert
Pâté ou terrine de campagne
Pâté breton
Terrine de canard
Pâté de lapin
Terrine de lapin
Pâté de gibier
Pâté ou terrine aux champignons (forestier)
Pâté de foie de porc, supérieur
Pâté de foie de porc
Mousse de foie de porc supérieure ou Crème de foie
Mousse de foie de porc
Mousse de canard
Pâté de foie de volaille
Pâté de foie d oie
Pâté (aliment moyen)
Pâté en croûte
Rillettes traditionnelles de porc
Rillettes pur porc
Rillettes de Tours
Rillettes du Mans
Rillettes pur oie
Rillettes de canard
Rillettes d oie
Rillettes de poulet
Quenelle de veau, en sauce
Quenelle de volaille, crue
Quenelle de volaille, en sauce
Quenelle de poisson, en sauce
Quenelle de poisson, crue
Quenelle nature, crue
Confit de canard, viande (cuisse), sans peau, réchauffé
Confit de canard
Canard, magret fumé
Foie gras, canard, entier, cuit
Foie gras, canard, bloc, sans morceaux
Foie gras, canard, bloc, 30% de morceaux
Foie gras, canard, bloc, 50% de morceaux
Foie gras de canard, cru
Galantine (aliment moyen)
Roulade de porc pistachée
Jambon en croûte
Fromage de tête
Museau de porc vinaigrette
Museau de boeuf
Andouille
Andouille de Guéméné
Andouille, réchauffée à la poêle
Andouille de Vire
Andouillette, crue
Andouillette, sautée/poêlée
Andouillette de Troyes, crue
Boudin noir, rayon frais
Boudin noir, sauté/poêlé
Boudin noir ou blanc, sauté/poêlé (aliment moyen)
Boudin blanc truffé, cru
Museau de boeuf en vinaigrette
Lardon nature, cru
Poitrine de porc, fumée, crue
Bresaola
Lardon nature, cuit
Corned-beef, appertisé
Oreille de porc demi-sel
Pied de porc demi-sel
Poitrine de porc demi-sel
Lardon fumé, cru
Lardon fumé, cuit
Viande des Grisons
Haché de volaille
Rôti de volaille en salaison, cuit
Chorizo
Chorizo supérieur, doux ou fort, type saucisse sèche
Chorizo supérieur, doux ou fort, type charcuterie en tranches
Salami
Salami pur porc
Salami porc et boeuf
Salami type danois
Saucisson à l ail
Saucisson cuit pur porc
Saucisson de Paris
Saucisson de Paris, fumé
Saucisson brioché, cuit
Cervelas
Cervelas obernois
Cervelas à l ail, pur porc
Mortadelle
Mortadelle, pur porc
Mortadelle, porc et boeuf
Mortadelle pistachée pur porc
Haché à base de bœuf ou Préparation de viande hachée de boeuf, 15% MG, cru
Cordon bleu de volaille
Boeuf, boulettes cuites
Veau, escalope panée, cuite
Boulettes au porc et au bœuf (à la suédoise), crues
Boulettes au bœuf et à l agneau (type kefta), crues
Brochette de volaille
Brochette de boeuf
Brochette mixte de viande
Volaille, croquette panée ou nuggets
Brochette d agneau
Brochette de porc, crue
Poulet, croquette panée ou nuggets
Poulet, manchons marinés, rôtis/cuits au four
Poulet, escalope panée
Dinde, escalope viennoise ou milanaise ou escalope panée
Saumon, cuit, sans précision (aliment moyen)
Cabillaud, cuit, sans précision (aliment moyen)
Hareng fumé, filet, doux
Carrelet ou plie, cuit à la vapeur
Églefin, cuit à la vapeur
Hareng, frit
Hareng fumé, au naturel
Hareng, grillé/poêlé
Lieu noir, cuit
Limande-sole, cuite à la vapeur
Maquereau, rôti/cuit au four
Maquereau, frit
Merlan, frit
Merlan, cuit à la vapeur
Cabillaud, rôti/cuit au four
Morue, salée, bouillie/cuite à l eau
Cabillaud, cuit à la vapeur
Mulet, rôti/cuit au four
Raie, rôtie/cuite au four
Roussette ou petite roussette ou saumonette, cuite
Saumon fumé
Saumon, cuit à la vapeur
Thon, rôti/cuit au four
Sole, cuite à la vapeur
Sole, rôtie/cuite au four
Sole, bouillie/cuite à l eau
Sole, frite
Raie, cuite au court-bouillon
Lotte ou baudroie, grillée/poêlée
Maquereau, fumé
Haddock (fumé) ou églefin fumé
Espadon, rôti/cuit au four
Turbot, rôti/cuit au four
Merlu, cuit à l étouffée
Églefin, grillé/poêlé
Sardine, grillée
Vivaneau, cuit
Rascasse, cuite à la vapeur
Lieu ou colin d Alaska, fumé
Saumon, cuit au micro-ondes, élevage
Saumon, bouilli/cuit à l eau, élevage
Dorade grise, ou daurade grise, ou griset, rôtie/cuite au four
Saumon, grillé/poêlé
Saumon, élevage, rôti/cuit au four
Hareng fumé, à l huile
Julienne ou Lingue, cuite
Flétan du Groënland ou flétan noir ou flétan commun, cuit à la vapeur
Sole, poêlée
Anguille, cuite (aliment moyen)
Anguille, rôtie/cuite au four
Anguille, bouillie/cuite à l eau
Brochet, rôti/cuit au four
Carpe, rôtie/cuite au four
Perche, rôtie/cuite au four
Truite, rôtie/cuite au four
Truite, cuite à la vapeur
Truite arc en ciel, élevage, rôtie/cuite au four
Truite arc en ciel, élevage, cuite à la vapeur
Panga, Pangasius, ou poisson-chat du Mékong, filet, cuit
Truite d élevage, fumée
Bar commun ou loup, rôti/cuit au four
Bar rayé ou bar d Amérique, cru
Lieu ou colin d Alaska, cru
Flétan de l Atlantique ou flétan blanc, cru
Hareng, cru
Lotte ou baudroie, crue
Saumon, cru, élevage
Turbot sauvage, cru
Cabillaud, cru
Merlu, cru
Lieu noir, surgelé, cru
Merlu, filet, surgelé, cru
Maquereau, cru
Raie, crue
Thon, cru
Carrelet ou plie, cru
Limande, crue
Sole, crue
Rascasse, crue
Thon albacore ou thon jaune, cru
Sardine, crue
Thon listao ou Bonite à ventre rayé, cru
Thon rouge, cru
Bar commun ou loup, cru, sans précision
Roussette ou petite roussette ou saumonette, crue
Bar ou loup de l Atlantique, cru
Thon germon ou thon blanc, cru
Anchois commun, cru
Dorade royale, ou daurade ou vraie daurade, crue, sauvage
Espadon, cru
Éperlan, cru
Cardine franche, crue
Rouget-barbet de roche, cru
Dorade royale ou daurade ou vraie daurade, crue, élevage
Mulet, cru
Truite de mer, crue
Merlan, cru
Morue, salée, sèche
Dorade grise, ou daurade grise, ou griset, crue
Bogue, crue
Bonite, crue
Maquereau espagnol ou maquereau blanc ou billard, cru
Denté, cru
Saint-Pierre, cru
Grondin, cru
Corb, cru
Capelan, cru
Dorade rose, ou daurade rose, crue
Rouget-barbet de roche, vapeur
Saupe, crue
Chinchard, cru
Églefin, cru
Congre, cru
Grenadier (de roche), cru
Lieu jaune ou colin, cru
Julienne ou Lingue, crue
Grand sébaste, ou dorade sébaste, ou daurade sébaste, cru
Tacaud, cru
Lieu noir, cru
Limande-sole, crue
Foie de morue, cru
Vivaneau, cru
Lavaret, cru
Sabre, cru
Flétan du Groënland ou flétan noir ou flétan commun, cru
Carangue, cru
Coulirou, cru
Saumon, cru, sauvage
Requin, cru
Orphie commune, crue
Lompe, crue
Brème, cru
Omble chevalier, cru
Loup tacheté, cru
Sprat, cru
Turbot, cru
Corégone lavaret, cru
Mérou, cru
Lingue bleue ou Lingue, crue
Sole tropicale ou Sole langue, crue
Turbot d élevage, cru
Plie commune, crue
Bar commun ou loup (Méditerranée), cru, sauvage
Bar commun ou loup (Méditerranée), cru, élevage
Sébaste du nord, ou dorade sébaste, ou daurade sébaste, crue
Hoki, tout lieu de pêche, cru
Grenadier bleu ou hoki de Nouvelle-Zélande, cru
Grondin perlon, cru
Merlu blanc du Cap, surgelé, cru
Chinchard maigre, cru
Chinchard gras, cru
Hareng maigre, cru
Hareng gras, cru
Joëls (petits poissons entiers) pour friture, crus
Empereur, filet, sans peau, cru
Rouget-barbet, filet avec peau, surgelé, cru (Thaïlande, Sénégal…)
Carpe, crue, élevage
Truite d élevage, crue
Truite arc en ciel, crue, élevage
Perche, crue
Brochet, cru
Esturgeon, cru
Anguille, crue
Pangasius ou Poisson-chat, cru
Tilapia, cru
Truite saumonée, crue
Lotte de rivière, crue
Sandre, cru
Perche du Nil, crue
Coquille Saint-Jacques, noix et corail, cuite
Crevette, cuite
Homard, bouilli/cuit à l eau
Moule, bouillie/cuite à l eau
Bulot ou Buccin, cuit
Langouste, bouillie/cuite à l eau
Crabe ou Tourteau, bouilli/cuit à l eau
Clam, Praire ou Palourde, bouilli/cuit à l eau
Écrevisse, cuite
Calmar ou calamar ou encornet, bouilli/cuit à l eau
Fruits de mer (aliment moyen)
Poulpe, cuit
Fruits de mer, cuits, surgelés
Calmar ou calamar ou encornet, frit ou poêlé avec matière grasse
Escargot, sans matière grasse ajoutée, cuit
Calmar ou calamar ou encornet, cru
Coquille Saint-Jacques, noix et corail, crue
Escargot, cru
Homard, cru
Huître, sans précision, crue
Moule commune, crue
Langouste, crue
Seiche, crue
Clam, Praire ou Palourde, cru
Poulpe, cru
Bulot ou Buccin, cru
Crevette, crue
Langoustine, crue
Moule de Méditerranée, crue
Écrevisse, crue
Huître creuse, crue
Huître plate, crue
Crevette, surgelée, crue
Crabe, cru
Ormeau, cru
Coquille Saint-Jacques, noix, crue
Pecten d Amérique ou Peigne du canada, noix, crue
Pétoncle ou Peigne du Pérou, noix, crue
Crevette rose, crue
Grenouille, cuisse, crue
Rillettes de crabe
Rillettes de poisson
Rillettes de saumon
Rillettes de thon
Rillettes de maquereau
Terrine de poisson
Terrine de fruits de mer, avec ou sans poisson
Tarama, préemballé
Calmar ou Calamar ou encornet, à la romaine (beignet)
Crabe, miettes et ou pattes décortiquées, appertisé, égoutté
Langoustine, panée, frite
Beignet de crevette
Moule, appertisée, égouttée
Escargot en sauce au beurre persillé, cuit
Moules à la sauce catalane ou escabèche (tomate), appertisée, égouttée
Accra de poisson
Carpaccio de saumon avec marinade
Anchois, filets roulés aux câpres, semi-conserve, égoutté
Anchois, filets à l huile, semi-conserve, égoutté
Carrelet ou plie, pané, frit
Oeufs de lompe, semi-conserve
Caviar, semi-conserve
Hareng mariné ou rollmops
Limande-sole, panée, frite
Pilchard, sauce tomate, appertisé, égoutté
Poisson, croquette ou beignet ou nuggets, frit
Poisson pané, surgelé, cru
Poisson pané, frit
Sardine, à l huile, appertisée, égouttée
Sardine, sauce tomate, appertisée, égouttée
Thon, au naturel, appertisé, égoutté
Sardine, à l huile d olive, appertisée, égouttée
Surimi, bâtonnets, tranche ou râpé saveur crabe
Oeufs de cabillaud, fumés, semi-conserve
Thon à l huile, appertisé, égoutté
Thon germon ou thon blanc, cuit à la vapeur sous pression
Maquereau, filet sauce tomate, appertisé, égoutté
Maquereau, filet sauce moutarde, appertisé, égoutté
Maquereau, filet au vin blanc, appertisé, égoutté
Saumon, appertisé, égoutté
Maquereau, au naturel, appertisé, égoutté
Merlan, pané
Foie de morue, appertisé, égoutté
Anchois au sel (anchoité, semi-conserve)
Thon germon ou thon blanc, à l huile d olive, appertisé, égoutté
Thon albacore ou thon jaune, au naturel, appertisé, égoutté
Maquereau, mariné
Anchois commun, mariné
Sardine, filets sans arêtes à l huile d olive, appertisés, égouttés
Surimi, fourré au fromage
Miettes de thon à la tomate, appertisées
Thon, à la catalane ou à l escabèche (sauce tomate), appertisé
Miettes de thon à l huile, appertisées
Oeufs de saumon, semi-conserve
Oeuf, blanc (blanc d oeuf), cuit
Oeuf, jaune (jaune d oeuf), cuit
Oeuf, dur
Oeuf, poché
Oeuf, à la coque
Oeuf, brouillé, avec matière grasse
Oeuf, au plat, sans matière grasse
Oeuf, cru
Oeuf, blanc (blanc d oeuf), cru
Oeuf, jaune (jaune d oeuf), cru
Oeuf de caille, cru
Oeuf de cane, cru
Oeuf d oie, cru
Oeuf de dinde, cru
Oeuf, jaune (jaune d oeuf), en poudre
Oeuf, blanc (blanc d oeuf), en poudre
Oeuf, en poudre
Omelette au fromage
Omelette aux lardons
Omelette aux champignons
Omelette aux fines herbes
Tortilla espagnole aux oignons (omelette aux pommes de terre et oignons)
Omelette, garnitures diverses : légumes, fromages, viandes... (aliment moyen)
Lait entier, UHT
Lait entier, pasteurisé
Lait demi-écrémé, UHT
Lait demi-écrémé, pasteurisé
Lait demi-écrémé, UHT, enrichi en vitamines
Lait écrémé, UHT
Lait écrémé, pasteurisé
Lait demi-écrémé, à teneur réduite en lactose
Lait de chèvre, entier, UHT
Lait de chèvre, demi-écrémé, UHT
Lait de chèvre, entier, cru
Lait de jument, entier
Lait de brebis, entier
Lait en poudre, entier
Lait concentré non sucré, entier
Lait concentré sucré, entier
Lait en poudre, demi-écrémé
Lait en poudre, écrémé
Lait, teneur en matière grasse inconnue, UHT (aliment moyen)
Boisson lactée, lait fermenté ou yaourt à boire, aromatisé, sucré
Boisson lactée, lait fermenté ou yaourt à boire, aux fruits, sucré
Boisson lactée, lait fermenté ou yaourt à boire, nature, sucré
Boisson lactée, lait fermenté ou yaourt à boire, nature, sucré, au L Casei
Lait fermenté ou spécialité laitière type yaourt, aromatisé, sucré, au bifidus
Lait fermenté ou spécialité laitière type yaourt, aux fruits, sucré, au bifidus
Lait fermenté ou spécialité laitière type yaourt, nature, au bifidus
Yaourt à la grecque, au lait de brebis
Yaourt à la grecque, sur lit de fruits
Yaourt au lait de chèvre, nature, 5% MG environ
Yaourt, lait fermenté ou spécialité laitière, aux céréales, 0% MG
Yaourt, lait fermenté ou spécialité laitière, aromatisé, avec édulcorants, 0% MG
Yaourt, lait fermenté ou spécialité laitière, aromatisé, sucré
Yaourt, lait fermenté ou spécialité laitière, aromatisé, sucré, à la crème
Yaourt, lait fermenté ou spécialité laitière, aux céréales
Yaourt, lait fermenté ou spécialité laitière, aux copeaux de chocolat, à la crème, sucré
Yaourt, lait fermenté ou spécialité laitière, aux fruits, avec édulcorants, 0% MG
Yaourt, lait fermenté ou spécialité laitière, aux fruits, avec édulcorants, 0% MG, enrichi en vitamine D
Yaourt, lait fermenté ou spécialité laitière, aux fruits, sucré
Yaourt, lait fermenté ou spécialité laitière, aux fruits, sucré, à la crème
Yaourt, lait fermenté ou spécialité laitière, aux fruits, sucré, enrichi en vitamine D
Yaourt, lait fermenté ou spécialité laitière, nature
Yaourt, lait fermenté ou spécialité laitière, nature, 0% MG
Yaourt, lait fermenté ou spécialité laitière, nature, à la crème
Yaourt, lait fermenté ou spécialité laitière, nature, sucré
Yaourt ou spécialité laitière nature (aliment moyen)
Yaourt ou spécialité laitière nature ou aux fruits (aliment moyen)
Spécialité laitière type encas, riche en protéines, sur lit de fruits, sucrée
Yaourt, lait fermenté ou spécialité laitière, aromatisé ou aux fruits, 0% MG (aliment moyen)
Yaourt, lait fermenté ou spécialité laitière, aromatisé ou aux fruits, non allégé en MG (aliment moyen)
Yaourt, lait fermenté ou spécialité laitière, aromatisé ou aux fruits, avec édulcorants (aliment moyen)
Yaourt, lait fermenté ou spécialité laitière, aromatisé ou aux fruits, sucré (aliment moyen)
Yaourt, lait fermenté ou spécialité laitière, aromatisé ou aux fruits, sucré, non allégé en MG (aliment moyen)
Yaourt, lait fermenté ou spécialité laitière, aromatisé ou aux fruits (aliment moyen)
Lait fermenté à boire, nature, maigre
Lait fermenté à boire, nature, au lait entier
Yaourt à la grecque, nature
Kéfir
Fromage blanc nature ou aux fruits (aliment moyen)
Faisselle, 6% MG environ
Fromage blanc nature, 0% MG
Fromage blanc nature, 3% MG environ
Fromage blanc nature, gourmand, 8% MG environ
Fromage blanc ou spécialité laitière, aux fruits, sucré, gourmand, 7% MG environ
Fromage frais type petit suisse, aux fruits, 2-3% MG
Fromage frais type petit suisse, aux fruits, 2-3% MG, enrichi en calcium et vitamine D
Fromage frais type petit suisse, nature, 0% MG
Fromage frais type petit suisse, nature, 4% MG environ
Fromage frais type petit suisse, nature, 10% MG environ
Fromage frais type petit suisse, aromatisé aux fruits, 2-3% MG, enrichi en calcium et vitamine D
Crème dessert, allégée en MG, rayon frais
Flan aux œufs, rayon frais
Lait emprésuré aromatisé, rayon frais
Lait gélifié aromatisé, nappé caramel, rayon frais
Lait gélifié aromatisé, rayon frais
Liégeois ou viennois (chocolat, café, caramel ou vanille), rayon frais
Lait gélifié aromatisé, allégé en matière grasse et en sucre, rayon frais
Panna cotta, rayon frais
Gâteau de semoule aux raisins et caramel, rayon frais
Gâteau de riz au caramel, rayon frais
Milk-shake, provenant de fast food
Crème dessert au chocolat, rayon frais
Mousse au chocolat (base laitière), rayon frais
Crème caramel, rayon frais
Crème aux œufs (petit pot de crème chocolat, vanille, etc.), rayon frais
Riz au lait, rayon frais
Crème brûlée, rayon frais
Crème dessert à la vanille, appertisée
Ile flottante, rayon frais
Semoule au lait, rayon frais
Mousse aux fruits, rayon frais
Crème dessert à la vanille, rayon frais
Crème dessert, rayon frais (aliment moyen)
Mousse liégeoise (chocolat, café, caramel ou vanille), rayon frais
Préparation pour mousse au chocolat
Crème dessert au café, rayon frais
Crème dessert au caramel, rayon frais
Crème dessert, appertisée (aliment moyen)
Crème dessert au chocolat, appertisée
Crème dessert, rayon frais ou appertisée (aliment moyen)
Cheesecake ou Gâteau au fromage frais, rayon frais
Dessert au soja, aux fruits, rayon frais
Dessert au soja, nature, rayon frais
Tiramisu, rayon frais
Mousse à la crème de marrons, rayon frais
Dessert au soja aromatisé, rayon frais
Profiteroles (crème pâtissière et sauce chocolat), rayon frais
Gâteau de semoule, appertisé
Mousse au chocolat traditionnelle, rayon frais
Clafoutis aux fruits, rayon frais
Liégeois aux fruits
Gâteau de riz, appertisé
Gâteau au chocolat, cœur fondant, rayon frais
Pain perdu
Lait de poule, sans alcool
Crème pâtissière
Fromage (aliment moyen)
Camembert, sans précision
Fromage à pâte molle et croûte fleurie (type camembert)
Camembert au lait cru
Fromage à pâte molle double crème environ 30% MG
Fromage rond à pâte molle et croûte fleurie 5 à 11% MG type camembert allégé en matière grasse
Coulommiers
Fromage rond à pâte molle et croûte fleurie environ 11% MG type coulommiers allégé en matière grasse
Fromage rond à pâte molle et croûte fleurie environ 5% MG type camembert allégé en matière grasse
Brie, sans précision
Brie de Meaux
Brie de Melun
Carré de l Est
Chaource
Maroilles laitier
Maroilles fermier
Neufchâtel
Fromage à pâte molle triple crème environ 40% MG
Fromage à pâte molle et croûte lavée (aliment moyen)
Fromage à pâte molle et croûte lavée, allégé environ 13% MG
Maroilles, sans précision
Livarot
Époisses
Munster
Langres
Pont l Évêque
Reblochon
Fromage à pâte molle à croûte lavée, au lait pasteurisé (type Vieux Pané)
Saint-Marcellin
Fromage à pâte molle et croûte mixte (lavée et fleurie) colorée
Mont d or ou Vacherin du Haut-Doubs (produit en France) ou Vacherin-Mont d Or (produit en Suisse)
Saint-Félicien
Fromage de chèvre lactique affiné, au lait cru (type Crottin de Chavignol, Picodon, Rocamadour, Sainte-Maure de Touraine)
Fromage de chèvre lactique affiné, au lait pasteurisé (type bûchette ou crottin)
Fromage de chèvre lactique affiné (type bûchette, crottin, Sainte-Maure)
Sainte-Maure de Touraine (fromage de chèvre)
Fromage de chèvre demi-sec
Fromage de chèvre bûche
Fromage de chèvre bûche, allégé en matière grasse
Fromage de chèvre à pâte molle non pressée non cuite croûte naturelle, au lait pasteurisé
Fromage de chèvre sec
Fromage de chèvre à pâte molle et croûte fleurie type camembert
Fromage de brebis à pâte molle et croûte fleurie
Chabichou (fromage de chèvre)
Pélardon (fromage de chèvre)
Crottin de chèvre, au lait cru
Crottin de chèvre, sans précision
Crottin de Chavignol (fromage de chèvre)
Picodon (fromage de chèvre)
Pouligny Saint-Pierre (fromage de chèvre)
Sainte-Maure (fromage de chèvre)
Selles-sur-Cher (fromage de chèvre)
Chevrot (fromage de chèvre)
Rocamadour (fromage de chèvre)
Valençay (fromage de chèvre)
Roquefort
Fourme de Montbrison
Fromage bleu au lait de vache
Fromage bleu d Auvergne
Fourme d Ambert
Fromage bleu des Causses
Gorgonzola
Bleu de Gex ou Fromage bleu du Haut-jura ou Bleu de septmoncel (AOC)
Fromage bleu de Bresse
Fromage bleu de Bresse allegé environ 15% MG
Fromage à pâte pressée cuite (aliment moyen)
Beaufort
Comté
Abondance
Gruyère IGP France
Gruyère
Emmental ou emmenthal
Fromage à pate pressée cuite type emmental ou emmenthal, allégé en matière grasse
Emmental ou emmenthal râpé
Ossau-Iraty
Parmesan
Fontina
Grana Padano
Fromage à pâte ferme environ 14% MG type Masdaam à teneur réduite en MG
Provolone
Cantal entre-deux
Cantal, Salers ou Laguiole
Salers
Cheddar
Edam
Mimolette jeune
Gouda
Mimolette demi-vieille
Mimolette vieille
Mimolette, sans précision
Fromage à pâte ferme environ 27% MG type Maasdam
Mimolette extra-vieille
Morbier
Fromage de brebis des Pyrénées
Saint-Nectaire, laitier
Raclette (fromage)
Saint-Nectaire, fermier
Saint-Nectaire, sans précision
Saint-Paulin (fromage à pâte pressée non cuite demi-ferme)
Tomme ou tome de vache
Tomme ou tome de montagne ou de Savoie
Tomme ou tome, allégée en matière grasse, environ 13% MG
Asiago
Fromage de brebis Corse à pâte molle
Tome des Bauges
Fromage de brebis à pâte pressée
Fromage de lactosérum de brebis
Fromage fondu en tranchettes
Fromage fondu en portions ou en cubes environ 20% MG
Fromage fondu double crème, environ 31% MG
Cancoillotte (spécialité fromagère fondue)
Spécialité fromagère fondante au fromage blanc et aux noix
Snack pour enfants à base de fromage fondu et de gressins
Fromage type feta, au lait de vache
Feta de brebis
Fromage type feta, au lait de vache, à l huile et aux aromates
Spécialité fromagère non affinée environ 25% MG, type fromage en barquette à tartiner ou coque fromagère
Spécialité fromagère non affinée environ 20% MG, type fromage en barquette à tartiner ou coque fromagère
Fromage de chèvre frais, au lait pasteurisé (type bûchette fraîche)
Fromage de chèvre frais, au lait cru (type palet ou crottin frais)
Fromage de chèvre frais, au lait pasteurisé ou cru (type crottin frais ou bûchette fraîche)
Fromage de chèvre à tartiner, nature
Spécialité fromagère non affinée à tartiner environ 30-40 % MG aromatisée (ex: ail et fines herbes)
Mascarpone
Ricotta
Mozzarella au lait de vache
Crème de lait, 30% MG, épaisse, rayon frais
Crème de lait, 30% MG, semi-épaisse, UHT
Crème chantilly, sous pression, UHT
Crème de lait, 15 à 20% MG, légère, semi-épaisse, UHT
Crème de lait, 15 à 20% MG, légère, épaisse, rayon frais
Spécialité à base de crème légère 8% MG, fluide ou épaisse
Boisson à base de riz, nature
Eau de source, embouteillée (aliment moyen)
Eau minérale, embouteillée, faiblement minéralisée (aliment moyen)
Eau minérale (aliment moyen)
Eau minérale, plate (aliment moyen)
Eau minérale, gazeuse (aliment moyen)
Eau du robinet
Eau embouteillée de source
Eau minérale Abatilles, embouteillée, non gazeuse, faiblement minéralisée (Arcachon, 33)
Eau minérale Aix-les-Bains, embouteillée, non gazeuse, faiblement minéralisée (Aix-les-Bains, 73)
Eau minérale Aizac, embouteillée, gazeuse, faiblement minéralisée (Aizac, 07)
Eau minérale Amanda, embouteillée, non gazeuse, fortement minéralisée (St-Amand, 59)
Eau minérale Arcens, embouteillée, gazeuse, moyennement minéralisée (Arcens, 07)
Eau minérale Ardesy, embouteillée, gazeuse, fortement minéralisée (Ardes, 63)
Eau minérale Auvergne, embouteillée, gazeuse, fortement minéralisée (Cornillon, 38)
Eau minérale Celtic, embouteillée, gazeuse ou non gazeuse, très faiblement minéralisée (Niederbronn, 67)
Eau minérale Chambon, embouteillée, non gazeuse, faiblement minéralisée (Chambon, 45)
Eau minérale Chantemerle, embouteillée, non gazeuse, faiblement minéralisée (Le Pestrin, 07)
Eau minérale Chateauneuf, embouteillée, gazeuse, fortement minéralisée (Chateauneuf, 63)
Eau minérale Chateldon, embouteillée, gazeuse, fortement minéralisée (Chateldon, 63)
Eau minérale Clos de l Abbaye, embouteillée, non gazeuse, moyennement minéralisée (St-Amand, 59)
Eau minérale Contrex, embouteillée, non gazeuse, fortement minéralisée (Contrexéville, 88)
Eau minérale Dax, embouteillée, non gazeuse, moyennement minéralisée (Dax, 40)
Eau minérale Didier, embouteillée, gazeuse, fortement minéralisée (Martinique)
Eau minérale Didier, embouteillée non gazeuse, fortement minéralisée (Martinique)
Eau minérale Evian, embouteillée, non gazeuse, faiblement minéralisée (Evian, 74)
Eau minérale Hépar, embouteillée, non gazeuse, fortement minéralisée (Vittel, 88)
Eau minérale Hydroxydase, embouteillée, gazeuse, fortement minéralisée (Le Breuil sur Couze, 63)
Eau minérale Vernière, embouteillée, gazeuse, moyennement minéralisée (Les Aires, 34)
Eau minérale Luchon, embouteillée, non gazeuse, faiblement minéralisée (Luchon, 31)
Eau minérale Mont-Roucous, embouteillée, très faiblement minéralisée (Lacaune, 81)
Eau de source Ogeu, embouteillée, faiblement minéralisée (Ogeu, 64)
Eau minérale Orée du bois, embouteillée, non gazeuse, moyennement minéralisée (St-Amand, 59)
Eau minérale Orezza, embouteillée, gazeuse, moyennement minéralisée (Rapaggio, 20B)
Eau minérale Parot, embouteillée, gazeuse, moyennement minéralisée (St-Romain-le-Puy, 42)
Eau minérale Plancoet, embouteillée, gazeuse ou non gazeuse, faiblement minéralisée (Plancoet, 22)
Eau minérale Propiac, embouteillée, non gazeuse, fortement minéralisée (Propiac, 26)
Eau minérale Puits St-Georges, embouteillée, gazeuse, moyennement minéralisée (St-Romain-le-Puy, 42)
Eau minérale Quézac, embouteillée, gazeuse, moyennement minéralisée (Quézac, 48)
Eau minérale Reine des basaltes, embouteillée, gazeuse, moyennement minéralisée (Asperjoc, 07)
Eau minérale Rozana, embouteillée, gazeuse, fortement minéralisée (Beauregard, 63)
Eau minérale Sail-les-Bains, embouteillée, non gazeuse, faiblement minéralisée (Sail-les-Bains, 42)
Eau minérale Salvetat, embouteillée, gazeuse, moyennement minéralisée (La Salvetat, 34)
Eau minérale Soultzmatt , embouteillée, gazeuse, moyennement minéralisée ( Soultzmatt, 68)
Eau minérale St-Amand, embouteillée, gazeuse ou non gazeuse, moyennement minéralisée (St-Amand, 59)
Eau minérale St-Antonin, embouteillée, non gazeuse, fortement minéralisée (St-Antonin-Noble-Val, 82)
Eau minérale St-Diéry, embouteillée, gazeuse, fortement minéralisée (St-Diéry, 63)
Eau minérale Ste-Marguerite, embouteillée, gazeuse, moyennement minéralisée (St-Maurice, 63)
Eau minérale St-Yorre, embouteillée, gazeuse, fortement minéralisée (Saint-Yorre, 03)
Eau minérale Thonon, embouteillée, non gazeuse, faiblement minéralisée (Thonon, 74)
Eau minérale Ventadour, embouteillée, gazeuse, faiblement minéralisée (Le Pestrin, 07)
Eau minérale Vernet, embouteillée, gazeuse, faiblement minéralisée (Prades, 07)
Eau minérale Vichy Célestins, embouteillée, gazeuse, fortement minéralisée (Saint-Yorre, 03)
Eau minérale Vittel, embouteillée, non gazeuse, moyennement minéralisée (Vittel, 88)
Eau minérale Volvic, embouteillée, non gazeuse, faiblement minéralisée (Volvic, 63)
Eau minérale Volvic active, embouteillée, gazeuse, faiblement minéralisée (Volvic, 63)
Eau minérale Wattwiller, embouteillée, gazeuse ou non gazeuse, faiblement minéraliséee (Wattwiller, 68)
Eau minérale Perrier, embouteillée, gazeuse, faiblement minéralisée (Vergèse, 30)
Eau minérale Badoit, embouteillée, gazeuse, moyennement minéralisée (St-Galmier, 42)
Eau minérale Avra, embouteillée, non gazeuse, faiblement minéralisée (Grèce)
Eau minérale Beckerich, embouteillée, non gazeuse, faiblement minéralisée (Luxembourg)
Eau minérale Caramulo, embouteillée, non gazeuse, très faiblement minéralisée (Portugal)
Eau minérale Chaudfontaine, embouteillée, non gazeuse, faiblement minéralisée (Belgique)
Eau minérale Christinen Brunnen, embouteillée, non gazeuse, moyennement minéralisée (Allemagne)
Eau minérale Courmayeur, embouteillée, non gazeuse, fortement minéralisée (Italie)
Eau minérale Highland spring, embouteillée, non gazeuse, faiblement minéralisée (Écosse)
Eau minérale Levissima, embouteillée, non gazeuse, faiblement minéralisée (Italie)
Eau minérale Luso, embouteillée, non gazeuse, très faiblement minéralisée (Portugal)
Eau minérale Néro, embouteillée, non gazeuse, faiblement minéralisée (Grèce)
Eau minérale Penacova, embouteillée, non gazeuse, très faiblement minéralisée (Portugal)
Eau minérale San Benedetto, embouteillée, faiblement minéralisée (Italie)
Eau minérale San Bernardo, embouteillée, très faiblement minéralisée (Italie)
Eau minérale San Pellegrino, embouteillée, gazeuse, moyennement minéralisée (Italie)
Eau minérale Spa-Reine, embouteillée, gazeuse ou non non gazeuse, moyennement minéralisée (Belgique)
Eau minérale Talians, embouteillée, non gazeuse, fortement minéralisée (Italie)
Eau minérale Valvert, embouteillée, non gazeuse, faiblement minéralisée (Belgique)
Eau minérale Appollinaris, embouteillée, non gazeuse, fortement minéralisée (Allemagne)
Eau de source Cristaline, embouteillée, non gazeuse
Eau minérale Biovive, embouteillée, non gazeuse, faiblement minéralisée (Dax, 40)
Eau minérale La Cairolle, embouteillée, non gazeuse, fortement minéralisée (Les Aires, 34)
Eau minérale Cilaos, embouteillée, gazeuse, fortement minéralisée (Cilaos, 974)
Eau minérale La Française, embouteillée, non gazeuse, fortement minéralisée (Propiac, 26)
Eau minérale Montcalm, embouteillée, non gazeuse, très faiblement minéralisée (Auzat, 09)
Eau minérale Montclar, embouteillée, non gazeuse, faiblement minéralisée (Montclar, 04)
Eau minérale Nessel, embouteillée, gazeuse, moyennement minéralisée (Soultzmatt, 68)
Eau minérale Ogeu, embouteillée, gazeuse, faiblement minéralisée (Ogeu-les-Bains, 64)
Eau minérale Ogeu, embouteillée, non gazeuse, faiblement minéralisée (Ogeu-les-Bains, 64)
Eau minérale Prince Noir, embouteillée, non gazeuse, fortement minéralisée (St-Antonin-Noble-Val, 82)
Eau minérale St-Alban, embouteillée, gazeuse, moyennement minéralisée (St-Alban, 42)
Eau minérale St-Géron, embouteillée, gazeuse, moyennement minéralisée (St-Géron, 43)
Eau minérale St-Michel-de-Mourcairol, embouteillée, gazeuse, moyennement minéralisée (Les Aires, 34)
Eau minérale Treignac, embouteillée, non gazeuse, très faiblement minéralisée (Treignac, 19)
Eau minérale Vals, embouteillée, gazeuse, moyennement minéralisée (Vals-les-Bains, 07)
Eau minérale Vauban, embouteillée, non gazeuse, moyennement minéralisée (St-Amand-les-Eaux, 59)
Eau minérale Carola, embouteillée, gazeuse ou non gazeuse, moyennement minéralisée (Ribeauville, 68)
Eau minérale Mont-Blanc, embouteillée, non gazeuse, faiblement minéralisée (Italie)
Eau minérale Eden (La Goa), embouteillée, non gazeuse, faiblement minéralisée (Suisse)
Jus d ananas, à base de concentré
Jus multifruit, pur jus, multivitaminé
Jus de fruits (aliment moyen)
Jus de carotte, pur jus
Jus de citron, maison
Jus multifruit - base orange, multivitaminé
Jus d orange, à base de concentré
Jus d orange, maison
Jus de pomme, à base de concentré
Jus de pamplemousse (pomélo), à base de concentré
Jus de raisin, pur jus
Jus de grenade, pur jus
Jus de pruneau
Jus de raisin, à base de concentré
Jus de mangue, frais
Jus de fruit de la passion ou maracudja, frais
Jus de pamplemousse (pomélo), maison
Jus de tomate, pur jus, salé à 3 g/L
Jus de pamplemousse (pomelo), pur jus
Jus de citron, pur jus
Jus de citron vert, maison
Jus de citron vert, pur jus
Jus de tomate, pur jus, salé à 6g/L
Jus de clémentine ou mandarine, pur jus
Jus multifruit, pur jus, standard
Jus de grenade, frais
Jus de tomate, pur jus (aliment moyen)
Jus multifruit, à base de concentré, standard
Jus d orange, pur jus
Jus de fruits, pur jus (aliment moyen)
Jus d ananas, pur jus
Jus de pomme, pur jus
Jus de légumes, pur jus (aliment moyen)
Jus de fruits, à base de concentré (aliment moyen)
Smoothie
Nectar d abricot
Nectar de papaye
Nectar de poire
Nectar multifruit, multivitaminé
Nectar multifruit, standard
Nectar de fruit de la passion ou maracuja
Nectar de banane
Nectar de goyave
Nectar de mangue
Nectar de pêche
Nectar d orange
Boisson gazeuse, sans jus de fruit, non sucrée, avec édulcorants
Limonade, sucrée
Eau de coco
Boisson à l eau minérale ou de source, aromatisée, sucrée
Tonic ou bitter, non sucré, avec édulcorants
Tonic ou bitter, sucré, avec édulcorants
Boisson au thé, aromatisée, sucrée, avec édulcorants
Limonade, sucrée, avec édulcorants
Cola, sucré
Boisson gazeuse aux fruits (de 10 à 50% de jus), sucrée
Boisson plate aux fruits, (à moins de 10% de jus), non sucrée, avec édulcorants
Boisson plate aux fruits (à moins de 10% de jus), sucrée
Boisson gazeuse, sans jus de fruit, sucrée
Boisson à l eau minérale ou de source, aromatisée, non sucrée, sans édulcorant
Boisson rafraîchissante sans alcool (aliment moyen)
Boisson à l eau minérale ou de source, aromatisée, non sucrée, avec édulcorants
Boisson gazeuse, sans jus de fruit, à teneur réduite en sucres
Boisson gazeuse aux fruits (à moins de 10% de jus), sucrée, avec édulcorants
Limonade, non sucrée, avec édulcorants
Cola, sucré, avec édulcorants
Diabolo (limonade et sirop)
Boisson gazeuse aux fruits (teneur en jus non spécifiée), sucrée (aliment moyen)
Boisson gazeuse aux fruits (à moins de 10% de jus), sucrée
Boisson préparée à partir de sirop à diluer type menthe, fraise, etc, sucré, dilué dans l eau
Cola, non sucré, avec édulcorants
Boisson au thé, aromatisée, teneur en sucre et édulcorant inconnue (aliment moyen)
Cola, teneur en sucre et édulcorant inconnue (aliment moyen)
Boisson au thé, aromatisée, non sucrée, avec édulcorants
Cola, sucré, sans caféine
Cola, non sucré, avec édulcorants, sans caféine
Boisson au thé, aromatisée, sucrée
Boisson gazeuse, sans jus de fruit, sucrée, avec édulcorants
Boisson plate aux fruits (10 à 50% de jus), à teneur réduite en sucres
Boisson plate aux fruits (teneur en jus non spécifiée), sucrée
Boisson plate aux fruits (10 à 50% de jus), sucrée
Boisson gazeuse aux fruits (à moins de 10% de jus), non sucrée, avec édulcorants
Tonic ou bitter, sucré
Boisson gazeuse aux fruits (à moins de 10% de jus), non sucrée, sans édulcorant
Boisson énergisante, sucrée
Boisson énergisante, non sucrée, avec édulcorants
Boisson au jus de fruit et au lait
Boisson lactée aromatisée (arôme inconnu), sucrée, au lait partiellement écrémé, enrichie et/ou restaurée en vitamines et/ou minéraux (aliment moyen)
Boisson lactée aromatisée au chocolat, sucrée, au lait partiellement écrémé, enrichie et/ou restaurée en vitamines et/ou minéraux
Boisson lactée aromatisée à la fraise, sucrée, au lait partiellement écrémé, enrichie à la vitamine D
Lait de coco ou Crème de coco
Boisson à l amande
Boisson au soja, nature
Boisson au soja, nature, enrichie en calcium
Boisson au soja, aromatisée, sucrée
Boisson au soja, aromatisée, sucrée, enrichie en calcium
Boisson à base d avoine, nature
Boisson au soja et jus de fruits concentrés
Café, non instantané, non sucré, prêt à boire
Thé infusé, non sucré
Tisane infusée, non sucrée
Café décaféiné, non instantané, non sucré, prêt à boire
Café expresso, non instantané, non sucré, prêt à boire
Café décaféiné, instantané, non sucré, prêt à boire
Café, instantané, non sucré, prêt à boire
Boisson cacaotée ou au chocolat, instantanée, sucrée, prête à boire (reconstituée avec du lait demi-écrémé standard)
Boisson cacaotée ou au chocolat, instantanée, sucrée, enrichie en vitamines, prête à boire (reconstituée avec du lait demi-écrémé standard)
Café au lait, café crème ou cappuccino, instantané ou non, non sucré, prêt à boire
Chicorée et café, instantané, non sucré, prête à boire (reconstituée avec du lait demi-écrémé standard)
Thé noir, infusé, non sucré
Thé vert, infusé, non sucré
Thé oolong, infusé, non sucré
Chicorée, instantanée, non sucrée, prête à boire (reconstituée avec du lait demi-écrémé standard)
Chicorée et café, instantané, non sucré, prêt à boire (reconstituée avec de l eau)
Café, moulu
Café, poudre soluble
Sirop à diluer, sucré
Boisson concentrée à diluer, sans sucres ajoutés, avec édulcorants, type "sirop 0%"
Café, décaféiné, poudre soluble
Cacao, non sucré, poudre soluble
Poudre cacaotée ou au chocolat pour boisson, sucrée
Poudre maltée, cacaotée ou au chocolat pour boisson, sucrée, enrichie en vitamines et minéraux
Chicorée et café, poudre soluble
Chicorée, poudre soluble
Café au lait ou cappuccino, poudre soluble
Café au lait ou cappuccino au chocolat, poudre soluble
Poudre cacaotée ou au chocolat sucrée pour boisson, enrichie en vitamines et minéraux
Poudre cacaotée ou au chocolat pour boisson, sucrée, enrichie en vitamines
Citron ou Lime, spécialité à diluer pour boissons, sans sucres ajoutés
Vin doux
Pétillant de fruits
Vin blanc 11°
Vin blanc mousseux
Vin rouge 10°
Vin rouge 11°
Vin rouge 12°
Vin rosé 11°
Champagne
Vin rouge 13°
Vin blanc mousseux aromatisé
Vin (aliment moyen)
Vin blanc sec 11°
Bière brune
Bière "coeur de marché" (4-5° alcool)
Bière forte (>8° alcool)
Cidre brut
Cidre doux
Bière faiblement alcoolisée (3° alcool)
Bière "spéciale" (5-6° alcool)
Bière "de spécialités" ou d abbaye, régionales ou d une brasserie (degré d alcool variable)
Cidre traditionnel
Cidre bouché demi-sec
Cidre aromatisé (framboise)
Bière sans alcool (<1,2° alcool)
Pastis
Eau de vie
Gin
Liqueur
Rhum
Whisky
Apéritif à base de vin ou vermouth
Vodka
Apéritif anisé sans alcool
Cocktail à base de rhum
Cocktail à base de whisky
Alcool pur
Marsala
Marsala aux oeufs
Sangria
Kir (au vin blanc)
Kir royal (au champagne)
Crème de cassis
Cocktail type punch, 16% alcool
Eau de vie de vin, type armagnac, cognac
Saké ou Alcool de riz
Cocktail sans alcool (à base de jus de fruits et de sirop)
Panaché (limonade et bière)
Panaché préemballé (<1° alc.)
Édulcorant à l aspartame, en pastilles
Édulcorant à l aspartame, en poudre
Vermicelles multicolores
Miel
Sucre blanc
Sucre roux
Sirop d érable
Sucre vanillé
Édulcorant à la saccharine
Mélasse de canne
Sucre allégé à l aspartame
Fructose
Édulcorant à base d extrait de stévia
Sirop d agave
Barre chocolatée biscuitée
Barre chocolatée non biscuitée enrobée
Barre à la noix de coco, enrobée de chocolat
Chocolat au lait, tablette
Chocolat noir à moins de 70% de cacao, à croquer, tablette
Chocolat au lait aux céréales croustillantes, tablette
Chocolat blanc, tablette
Barres ou confiserie chocolatées au lait
Chocolat au lait aux fruits secs (noisettes, amandes, raisins, praline), tablette
Chocolat au lait sans sucres ajoutés, avec édulcorants, tablette
Chocolat blanc aux fruits secs (noisettes, amandes, raisins, praliné) , tablette
Chocolat noir sans sucres ajoutés, avec édulcorants, en tablette
Pâte à tartiner chocolat et noisette
Confiserie au chocolat dragéifiée
Cacahuètes enrobées de chocolat dragéifiées
Bouchée chocolat fourrage fruits à coques et/ou praliné
Rocher chocolat fourré praliné
Chocolat noir aux fruits (orange, framboise, poire), tablette
Chocolat noir aux fruits secs (noisettes, amandes, raisins, praline), tablette
Barre chocolatée aux fruits secs
Chocolat noir fourrage confiseur à la menthe
Barre goûter frais au lait et chocolat
Chocolat noir à 70% cacao minimum, extra, dégustation, tablette
Chocolat au lait fourré
Chocolat noir fourré praliné, tablette
Chocolat au lait fourré au praliné, tablette
Chocolat noir à 40% de cacao minimum, à pâtisser, tablette
Bonbon / bouchée au chocolat fourrage gaufrettes / biscuit
Barre chocolat au lait avec nougat
Barre goûter frais au lait et chocolat avec génoise
Chocolat, en tablette (aliment moyen)
Bonbons, tout type
Chewing-gum, sucré
Pâte de fruits
Zeste d orange confit
Marron glacé
Fruit confit
Nougat ou touron
Dragée amande
Guimauve ou marshmallow
Chewing-gum, sans sucre
Bonbon dur et sucette
Bonbon gélifié
Bonbon au caramel, mou
Calissons d Aix en Provence
Chewing-gum, teneur en sucre inconnue (aliment moyen)
Confiture de fraise (extra ou classique)
Confiture d abricot (extra ou classique)
Confiture de cerise (extra ou classique)
Marmelade d orange
Confiture de lait
Confiture de framboise (extra ou classique)
Confiture, tout type de fruits, allégée en sucres (extra ou classique)
Glace à l eau ou sorbet ou crème glacée, tout parfum (aliment moyen)
Barre glacée chocolatée
Glace ou crème glacée, bâtonnet, enrobé de chocolat
Glace ou crème glacée, cône (taille standard)
Glace ou crème glacée, en bac
Glace au yaourt
Glace ou crème glacée, gourmande, en bac
Glace ou crème glacée, gourmande, en pot
Glace ou crème glacée, mini cône
Glace ou crème glacée, pot individuel
Glace ou crème glacée, bac ou pot (aliment moyen)
Glace ou crème glacée, petit pot enfant
Sorbet, bâtonnet
Sorbet, en bac
Glace à l eau
Profiterole avec glace vanille et sauce chocolat
Pêche melba
Dessert glacé type mystère ou vacherin
Dessert glacé, type sundae
Dessert glacé feuilleté, à partager
Omelette norvégienne
Poire belle Hélène
Nougat glacé
Citron givré ou Orange givrée (sorbet)
Coupe glacée type café ou chocolat liégeois
Bûche glacée
Coupe glacée parfum pêche Melba ou poire Belle-Hélène
Beurre à 82% MG, doux
Huile de beurre ou Beurre concentré
Beurre à 80% MG, demi-sel
Beurre à 80% MG, salé
Beurre à 82% MG, doux, tendre
Beurre à 60-62% MG, à teneur réduite en matière grasse, doux
Beurre à 60-62% MG, à teneur réduite en matière grasse, demi-sel
Beurre ou assimilé allégé (léger ou à teneur reduite en matière grasse), doux (aliment moyen)
Beurre à teneur en matière grasse inconnue (allégé ou non), demi-sel (aliment moyen)
Beurre ou assimilé à teneur en matière grasse inconnue, doux (aliment moyen)
Beurre à 39-41% MG, léger, doux
Matière grasse laitière à 25% MG, légère, "à tartiner", doux
Matière grasse laitière à 20% MG, légère, "à tartiner", doux
Carotte, déshydratée
Beurre de cacao
Matière grasse ou graisse végétale solide (type margarine) pour friture
Huile pour friture, sans précision
Huile de palme, sans précision
Huile de palme raffinée
Huile végétale (aliment moyen)
Huile d amandes d abricot
Huile d amande
Huile d arachide
Huile d avocat
Huile de germe de blé
Huile de carthame
Huile de colza
Huile de coton
Huile de lin
Huile de maïs
Huile de noisette
Huile de noix
Huile d olive vierge extra
Huile de pépins de raisin
Huile de sésame
Huile de soja
Huile de tournesol
Huile combinée (mélange d huiles)
Huile combinée, mélange d huile d olive et de graines
Huile d argan ou d argane
Matière grasse végétale (type margarine) à 80% MG, salé
Matière grasse végétale ou margarine, 80% MG, doux
Matière grasse végétale (type margarine) à 70% MG, doux
Matière grasse végétale (type margarine) à 60% de MG, allégée, au tournesol, doux
Matière grasse végétale (type margarine), teneur en matière grasse inconue, doux (aliment moyen)
Matière grasse végétale (type margarine), teneur réduite en matière grasse inconnue, doux (aliment moyen)
Matière grasse végétale (type margarine), à tartiner, à 30-40% MG, légère, doux
Matière grasse végétale (type margarine), à tartiner, à 30-40% MG, légère, demi-sel
Matière grasse végétale (type margarine), à tartiner, à 30-40% MG, légère, doux, aux esters de stérol végétal
Matière grasse végétale (type margarine), à tartiner, à 30-40% MG, légère, doux, riche en oméga 3
Matière grasse végétale (type margarine) à 50-63% MG, allégée, doux, riche en oméga 3
Matière grasse végétale (type margarine) à 50-63% MG, allégée, demi-sel
Matière grasse végétale (type margarine) à 50-63% MG, allégée, doux, aux esters de stérol végétal
Matière grasse végétale (type margarine) à 50-63% MG, allégée, demi-sel, riche en oméga 3
Matière grasse végétale (type margarine) à 50-63% MG, allégée, doux
Matière grasse végétale (type margarine) à 30-40% MG, légère, demi-sel, aux esters de stérol végétal
Matière grasse mélangée (végétale et laitière) à 50-63% MG
Matière grasse mélangée (végétale et laitière) à 50-63% MG, demi-sel
Matière grasse mélangée (végétale et laitière), à tartiner, à 30-40% MG
Matière grasse mélangée (végétale et laitière), à tartiner, à 30-40% MG, demi-sel
Huile de foie de morue
Huile de sardine
Huile de saumon
Huile de hareng
Saindoux
Lard gras, cru
Graisse de poulet
Graisse de canard
Graisse d oie
Graisse de dinde
Huile de paraffine
Ketchup
Sauce tartare, préemballée
Mayonnaise (70% MG min.)
Mayonnaise à teneur réduite en matière grasse ou Mayonnaise allégée
Sauce barbecue, préemballée
Sauce soja, préemballée
Sauce vinaigrette allégée en MG (25 à 50% d huile), préemballée
Sauce vinaigrette (50 à 75% d huile), préemballée
Harissa (sauce condimentaire)
Sauce bourguignonne, préemballée
Sauce moutarde, préemballée
Sauce au yaourt
Sauce américaine, préemballée
Sauce aïoli, préemballée
Sauce crudités ou Sauce salade, préemballée
Sauce Nuoc Mâm ou Sauce au poisson, préemballée
Sauce burger, préemballée
Sauce crudités ou Sauce salade, allégée en matière grasse, préemballée
Sauce kebab, préemballée
Sauce rouille, préemballée
Sauce au poivre, condimentaire, froide, préemballée
Caviar de tomates
Sauce teriyaki, préemballée
Guacamole, préemballé
Houmous
Sauce béchamel, préemballée
Sauce béarnaise, préemballée
Sauce hollandaise, préemballée
Sauce tomate aux oignons, préemballée
Sauce armoricaine, préemballée
Sauce tomate à la viande ou Sauce bolognaise, préemballée
Sauce au poivre vert, préemballée
Sauce madère, préemballée
Sauce à l échalote à la crème, préemballée
Sauce carbonara, préemballée
Sauce chasseur, préemballée
Sauce au curry, préemballée
Sauce au beurre blanc, préemballée
Sauce béchamel, maison
Sauce au beurre, préemballée
Sauce à la crème
Sauce aux champignons, préemballée
Sauce à la crème aux épices
Sauce à la crème aux herbes
Sauce aigre douce, préemballée
Sauce au vin rouge
Sauce basquaise ou Sauce aux poivrons, préemballée
Sauce tomate aux champignons, préemballée
Sauce tomate aux olives, préemballée
Sauce pesto, préemballée
Sauce au poivre, chaude, préemballée
Sauce au fromage pour risotto ou pâtes, préemballée
Sauce au roquefort, préemballée
Sauce aux champignons et à la crème, préemballée
Sauce grand veneur, préemballée
Sauce indienne type tandoori ou tikka masala, préemballée
Sauce tomate aux petits légumes, préemballée
Sauce tomate au fromage, préemballée
Sauce pesto rosso, préemballée
Sauce chaude (aliment moyen)
Meloukhia, sauce, artisanale
Sauce au chocolat
Crème anglaise, préemballée
Cornichon, au vinaigre
Moutarde
Vinaigre
Moutarde à l ancienne
Câpres, au vinaigre
Tapenade
Oignon au vinaigre
Vinaigre de cidre
Vinaigre balsamique
Cornichon, aigre-doux
Olive noire, en saumure
Olive verte, en saumure
Olive noire, à l huile (à la grecque)
Olives vertes, fourrées ou farcies (anchois, poivrons, etc.)
Bouillon de boeuf, déshydraté
Fond de veau pour sauces et cuisson, déshydraté
Fond de volaille pour sauces et cuisson, déshydraté
Court-bouillon pour poissons, déshydraté
Bouillon de volaille, déshydraté
Gelée au madère, déshydratée
Gelée au madère
Préparation culinaire à base de soja, type "crème de soja"
Fond de veau, préemballé
Pizza, sauce garniture pour
Bouillon de viande et légumes type pot-au-feu, déshydraté
Bouillon de viande et légumes type pot-au-feu, non dégraissé, déshydraté
Bouillon de viande et légumes type pot-au-feu, dégraissé, déshydraté
Sel blanc alimentaire, non iodé, non fluoré (marin, ignigène ou gemme)
Sel au céleri
Sel blanc alimentaire, iodé, non fluoré (marin, ignigène ou gemme)
Fleur de sel, non iodée, non fluorée
Sel marin gris, non iodé, non fluoré
Sel blanc alimentaire, iodé, fluoré à 25 mg /100g (marin, ignigène ou gemme)
Curry, poudre
Gingembre, poudre
Poivre noir, poudre
Poivre blanc, poudre
Poivre gris, poudre
Cannelle, poudre
Coriandre, graine
Safran
Cumin, graine
Noix de muscade
Paprika
Clou de girofle
Laurier, feuille
Quatre épices
Pavot, graine
Carvi, graine
Vanille, extrait alcoolique
Fenouil, graine
Gingembre, racine crue
Cardamome, poudre
Fenugrec, graine
Epice (aliment moyen)
Poivre de Cayenne ou piment de Cayenne
Curcuma, poudre
Vanille, extrait aqueux
Ail, cru
Cerfeuil, frais
Ciboule ou Ciboulette, fraîche
Persil, frais
Raifort, cru
Menthe, fraîche
Basilic, frais
Romarin, frais
Sauge, fraîche
Thym, frais
Herbes aromatiques fraîches (aliment moyen)
Estragon, frais
Aneth, frais
Coriandre, fraiche
Ail séché, poudre
Persil, séché
Menthe, séchée
Basilic, séché
Marjolaine, séchée
Origan, séché
Romarin, séché
Sauge, séchée
Thym, séché
Herbes de Provence, séchées
Sarriette, séchée
Meloukhia, feuilles de corète séchées, en poudre
Agar (algue), cru
Spiruline (Spirulina sp.), séchée ou déshydratée
Wakamé (Undaria pinnatifida), séchée ou déshydratée
Laitue de mer (Ulva sp.), séchée ou déshydratée
Kombu royal (Saccharina latissima), séchée ou déshydratée
Nori (Porphyra sp.), séchée ou déshydratée
Dulse (Palmaria palmata), séchée ou déshydratée
Kombu ou kombu japonais (Laminaria japonica), séchée ou déshydratée
Kombu breton (Laminaria digitata), séchée ou déshydratée
Haricot de mer (Himanthalia elongata), séchée ou déshydratée
Gracilaire ou ogonori (Gracilaria verrucosa), séchée ou déshydratée
Fucus vésiculeux (Fucus serratus ou Fucus vesiculosus), séché ou déshydraté
Ao-nori (Enteromorpha sp.), séchée ou déshydratée
Lichen de mer ou pioca ou goémon rouge (Chondrus crispus), séché ou déshydraté
Ascophylle noueux ou goémon noir (Ascophyllum nodosum), séché ou déshydraté
Wakamé atlantique (Alaria esculenta), séchée ou déshydratée
Boisson diététique pour le sport
Substitut de repas hypocalorique, crème dessert
Substitut de repas hypocalorique, prêt à boire
Substitut de repas hypocalorique, poudre reconstituée avec lait écrémé
Substitut de repas hypocalorique, poudre reconstituée avec lait écrémé, type milk-shake
Son de blé
Son d avoine
Son de maïs
Son de riz
Son (aliment moyen)
Germe de blé
Gélatine, sèche
Levure alimentaire
Levure de boulanger, compressée
Levure de boulanger, déshydratée
Levure chimique ou Poudre à lever
Bicarbonate de soude
Miso
Tempeh
Sirop pour fruits appertisés au sirop
Sirop léger pour fruits appertisés au sirop
Jus d ananas pour ananas appertisé au jus
Gélifiant pour confitures
Pollen, partiellement séché
Pollen,frais
Base de pizza à la crème
Base de pizza tomatée
Lécithine de soja
Lait 1er âge, poudre soluble (préparation pour nourrissons)
Lait 2e âge, poudre soluble (préparation de suite)
Boisson aux fruits pour bébé dès 4/6mois
Boisson à base de plantes pour bébé
Boisson infantile céréales lactées aux légumes pour dîner dès 4/6 mois
Boisson infantile céréales lactées aux fruits pour le goûter dès 4/6 mois
Boisson infantile céréales lactées pour le petit déjeuner dès 4/6 mois
Boisson infantile céréales lactées pour le petit déjeuner dès 8/9 mois
Boisson infantile céréales lactées pour le petit déjeuner dès 12 mois
Boisson infantile céréales lactées pour le petit déjeuner
Boisson infantile céréales lactées (aliment moyen)
Lait de croissance infantile, liquide (aliment lacté destiné aux enfants en bas âge)
Lait 1er âge, prêt à consommer (préparation pour nourrissons)
Lait 2e âge, prêt à consommer (préparation pour nourrissons)
Petit pot légumes, dès 4-6 mois
Petit pot légumes, avec féculent, dès 4/6 mois
Plat légumes, avec féculent, dès 6-8 mois
Plat légumes, avec féculent et lait/crème, dès 6-8 mois
Plat légumes, avec féculent et lait/crème, dès 8-12 mois
Plat légumes, avec féculent et lait/crème, dès 12 mois
Soupe pour bébé légumes et pomme de terre
Soupe pour bébé légumes, céréales et lait
Plat légumes, avec féculent et lait/crème, dès 18 mois
Plat légumes, avec féculent et viande/poisson, dès 6-8 mois
Plat légumes, avec féculent et viande/poisson, dès 8-12 mois
Plat légumes, avec féculent et viande/poisson, dès 12 mois
Plat légumes, avec féculent et viande/poisson, dès 18 mois
Petit pot fruit avec banane pour bébé
Petit pot fruit sans banane pour bébé
Dessert lacté infantile type crème dessert
Dessert lacté infantile au riz ou à la semoule
Dessert lacté infantile nature sucré ou aux fruits
Céréales instantanées, poudre à reconstituer, dès 4/6 mois
Céréales instantanées, poudre à reconstituer, dès 6 mois
Biscuit pour bébé
Poudre cacaotée pour bébé`.split('\n');